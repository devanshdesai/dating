/****** Object:  Table [dbo].[DeviceSettingsActivity]    Script Date: 20-02-2021 01:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[dbo].[DeviceSettingsActivity]') and TYPE = 'U')
BEGIN

CREATE TABLE [dbo].[DeviceSettingsActivity](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceSettingId] [bigint] NOT NULL,
	[AccountId] [bigint] NULL,
	[SerialNumber] [nvarchar](50) NULL,
	[ChannelName] [nvarchar](25) NULL,
	[Alias] [nvarchar](25) NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[IpAddress] [varchar](50) NULL,
	[Browser] [varchar](250) NULL,
	[OS] [varchar](250) NULL,
 CONSTRAINT [PK_DeviceSettingsActivity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
GO