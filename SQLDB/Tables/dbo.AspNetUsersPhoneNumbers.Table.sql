/****** Object:  Table [dbo].[AspNetUsersPhoneNumbers]    Script Date: 12-02-2021 07:19:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[dbo].[AspNetUsersPhoneNumbers]') and TYPE = 'U')
BEGIN

CREATE TABLE [dbo].[AspNetUsersPhoneNumbers](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AspNetUserId] [bigint] NOT NULL,
	[CountryCode] [varchar](20) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[IsVerified] [bit] NULL,
	[OtpCode] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_AspNetUsersPhoneNumbers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

END
GO

ALTER TABLE [dbo].[AspNetUsersPhoneNumbers] ADD  CONSTRAINT [DF_AspNetUsersPhoneNumbers_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO


