/****** Object:  Table [dbo].[AspNetUsersEmails]    Script Date: 12-02-2021 07:08:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[dbo].[AspNetUsersEmails]') and TYPE = 'U')
BEGIN

CREATE TABLE [dbo].[AspNetUsersEmails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AspNetUserId] [bigint] NULL,
	[Email] [nvarchar](256) NULL,
	[IsVerified] [bit] NULL,
	[OtpCode] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_AspNetUsersEmails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

END
GO