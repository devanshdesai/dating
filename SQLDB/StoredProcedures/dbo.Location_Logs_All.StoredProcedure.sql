/****** Object:  StoredProcedure [dbo].[Device_Log_All]    Script Date: 06-03-2021 12:36:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Location_Logs_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Location_Logs_All]
GO

CREATE PROCEDURE [dbo].[Location_Logs_All]
	@Search varchar(MAX) = '',
	@Offset int = 0,
	@Limit int = 0,
	@UserId bigint = 0,
	@AccountId bigint = 0,
	@ActionType varchar(50) = NULL,
	@FromDate DateTime = NULL,
	@ToDate DateTime = NULL
AS
BEGIN

		SET NOCOUNT ON;

		if(ISNULL(@ToDate,'') <> '')
		BEGIN
			select @ToDate = Convert(date,@ToDate)
			Select @ToDate = dateadd(hh,23,@ToDate)
			Select @ToDate = dateadd(mi,59,@ToDate)
			Select @ToDate = dateadd(ss,59,@ToDate)
		END

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[Location_Logs] L with(nolock)
								where (@UserId = 0 or  L.ActionBy = @UserId)
								AND L.ActionDate >= ISNULL(NULLIF(@FromDate,''),L.ActionDate)  AND L.ActionDate <= ISNULL(NULLIF(@ToDate,''),L.ActionDate)
								AND L.ActionType = ISNULL(NULLIF(@ActionType,''), L.ActionType)
								AND L.AccountID = ISNULL(NULLIF(@AccountId,0), L.AccountID)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(L.ActionBy) like '%'+lower(@Search)+'%')
									)
								)
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				*
			FROM [dbo].[Location_Logs] L with(nolock)
			where (@UserId = 0 or  L.ActionBy = @UserId)
			AND L.ActionDate >= ISNULL(NULLIF(@FromDate,''),L.ActionDate)  AND L.ActionDate <= ISNULL(NULLIF(@ToDate,''),L.ActionDate)
			AND L.ActionType = ISNULL(NULLIF(@ActionType,''), L.ActionType)
			AND L.AccountID = ISNULL(NULLIF(@AccountId,0), L.AccountID)
			and
			(
				isnull(@Search,'') = '' or
				(
					(lower(L.ActionBy) like '%'+lower(@Search)+'%')
				)
			)
			order by L.Id 
			
			desc
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
END
GO


