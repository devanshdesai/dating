/****** Object:  StoredProcedure [dbo].[EscalationData_UserEmailsByID]    Script Date: 12-02-2021 08:44:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[EscalationData_UserEmailsByID]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EscalationData_UserEmailsByID]
GO

CREATE Procedure [dbo].[EscalationData_UserEmailsByID]  
	@ID bigint
AS 
BEGIN
SET NOCOUNT ON;

SELECT
	ed.*,
	STUFF((select ',' + ISNULL(u.Email,'')
	from dbo.SplitString(ed.userids,',') ui
	inner join AspNetUsers u on u.ID = ui.item
	for xml path(''), type).value('.', 'NVARCHAR(MAX)'), 1, 1, '') as Emails
FROM EscalationData ed
WHERE id = @ID

END
GO


