/****** Object:  StoredProcedure [dbo].[LoginHistory_All]    Script Date: 12-02-2021 07:31:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[LoginHistory_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[LoginHistory_All]
GO

-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
-- exec LoginHistory_All '',0,100,0,1142,null,null
CREATE PROCEDURE [dbo].[LoginHistory_All]
(
    @Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT,
	@AccountId BIGINT = 0,
	@UserId BIGINT = 0,
	@FromDate DateTime = NULL,
	@ToDate DateTime = NULL
)
AS
BEGIN
    DECLARE @TotalRecords bigint

		IF(ISNULL(@ToDate,'') <> '')
		BEGIN
			select @ToDate = Convert(date,@ToDate)
			Select @ToDate = dateadd(hh,23,@ToDate)
			Select @ToDate = dateadd(mi,59,@ToDate)
			Select @ToDate = dateadd(ss,59,@ToDate)
		END

		

	SET @TotalRecords = (
						   SELECT 
								  COUNT(*) 
						   FROM 
						         LoginHistories
						   WHERE
								UserId=@UserId
								AND
								IsSuccess=1
								--AND 
								--((@FromDate != null AND [Timestamp] >= ISNULL(NULLIF(@FromDate,''),[Timestamp]))
								--AND (@ToDate != null AND [Timestamp] <= ISNULL(NULLIF(@ToDate,''),[Timestamp])))
						)

	IF @Limit = 0
	BEGIN
			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
				SET @Limit = 10
			END
	END 


	SELECT
	       Id,
		   UserId,
		   [Timestamp],
		   IpAddress,
		   IsSuccess
	FROM
	      LoginHistories
	WHERE
	      UserId=@UserId
		  AND
		  IsSuccess=1
		  --AND 
		  --[Timestamp] >= ISNULL(NULLIF(@FromDate,''),[Timestamp])  
		  --AND [Timestamp] <= ISNULL(NULLIF(@ToDate,''),[Timestamp])
	order by Id
    DESC
    OFFSET @offset rows fetch next @Limit rows only

	select @TotalRecords as TotalRecords
END
GO


