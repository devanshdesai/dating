/****** Object:  StoredProcedure [dbo].[APIJobMaster_All]    Script Date: 12-02-2021 08:48:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[APIJobMaster_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[APIJobMaster_All]
GO

CREATE PROCEDURE [dbo].[APIJobMaster_All]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
	       jm.ID,
		   jm.Title,
		   jm.Description,
		   jm.URL,
		   jm.Type,
		   jm.Body,
		   jm.Headers,
		   jm.IsActive,
		   jm.CreatedBy,
		   jm.CreatedDate,
		   jm.UpdatedBy,
		   jm.UpdatedDate,
		   jm.NeedToValidateBySMS,
		   jm.NeedToValidateByEmail,
		   jm.SMSIdentifyText,
		   jm.EmailIdentifyText
    FROM
	      APIJobMaster jm
	WHERE 
		  ISNULL(jm.IsActive,1)=1
END

GO


