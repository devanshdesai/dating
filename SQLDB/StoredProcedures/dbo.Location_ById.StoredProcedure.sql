/****** Object:  StoredProcedure [dbo].[Location_ById]    Script Date: 12-02-2021 06:59:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Location_ById]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Location_ById]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec Location_ById 1230,2,1646
CREATE PROCEDURE [dbo].[Location_ById]
@Id BIGINT = 0,
@AccountId BIGINT = 0,
@UserId BIGINT = 0
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 200,
	        @Message VARCHAR(250) = 'Data Found',
			@IsAdmin bit;
   

    SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

	IF @IsAdmin IS NULL OR @AccountId = 0
    BEGIN
         SET @AccountId = -1
    END
    ELSE IF @IsAdmin = 1
    BEGIN
         SET @UserId = 0
    END

	IF (SELECT COUNT(*) FROM [Location] WHERE Id=@Id AND AccountId = @AccountId AND IsActive = 1) = 0
	BEGIN
	     SET @Code = 400
		 SET @Message = 'Location Not Found'
		 SET @Id=0
		 SET @IsAdmin = 0
	END



    SELECT @Code as Code,
		   @Message as [Message]
	     
    SELECT  
	        LOC.Id
		   ,LOC.AccountId
		   ,LOC.Name
		   ,LOC.Description
		   ,LOC.Address1
		   ,LOC.Address2
		   ,LOC.City
		   ,LOC.State
		   ,LOC.Zip
		   ,LOC.Country
		   ,LOC.Timezone
		   ,LOC.IanaTimezoneId
		   ,LOC.ParentLocationId
		   ,LOC.CreatedAt
		   ,LOC.CreatedBy
		   ,LOC.ModifiedAt
		   ,LOC.ModifiedBy
		   ,LOC.IsActive
		   ,LOC.IsTransportLocation
		   ,(SELECT TOP 1 LocationPhoto FROM LocationImages WHERE LocationId=@Id ORDER BY Id DESC) as LocationPhoto
	FROM 
	       [Location] LOC
	WHERE 
	       LOC.Id = @Id 
		   AND LOC.AccountId = @AccountId
		   AND LOC.IsActive = 1
		   AND (@UserId = 0 OR (@UserId > 0 AND  LOC.Id IN (SELECT LocationId FROM UserLocationMap WHERE UserId=@UserId AND IsDeleted=0)))

	-- IF @UserId IS AN ADMIN -> SELECT THE USER DETAILS (Id,Fullname, Email) WHERE THE LOCATION ID IS EQUAL TO @LocationId FROM THE BRIDGE TABLE

	IF @IsAdmin = 1 
	BEGIN
	     SELECT 
                ANU.FirstName+' '+ANU.LastName as UserName,
				ANU.Email as UserEmail,
				ANU.Id as UserId,
				ANU.IsAdmin
		 FROM
		       UserLocationMap ULP
		 LEFT JOIN
		       AspNetUsers ANU
		 ON ANU.Id = ULP.UserId
		 LEFT  JOIN 
		       Location LOC
		 ON LOC.Id = ULP.LocationId
		 WHERE 
		       LOC.Id = @Id AND LOC.AccountId=@AccountId AND LOC.IsActive=1
		       AND
			   ULP.IsDeleted = 0
			   AND
			   ANU.IsDeactivated=0


		 SELECT 
		       Name,
			   SerialNumber
		 FROM
		       Device
		 WHERE 
		       AccountId=@AccountId
			   AND 
			   LocationId=@Id
	END
	ELSE
	BEGIN
	      SELECT 
		        '' as UserName,
				'' as UserEmail,
				'' as UserId
		  FROM   
		        UserLocationMap
		  WHERE  
		        Id = 0


		   SELECT 
		         Name,
			     SerialNumber
		   FROM
		         Device
		   WHERE 
		         AccountId=@AccountId
			     AND 
			     LocationId=@Id
	END
	
END
GO


