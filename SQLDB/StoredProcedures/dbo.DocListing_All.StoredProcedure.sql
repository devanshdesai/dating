/****** Object:  StoredProcedure [dbo].[DocListing_All]    Script Date: 19-02-2021 12:36:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[DocListing_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DocListing_All]
GO

CREATE PROCEDURE [dbo].[DocListing_All]
	@Search varchar(MAX),
	@Offset int,
	@Limit int,
	@AccountId bigint = 0,
	@UploadedBy bigint = 0
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[DocListing] D
								left join [AspNetUsers] AU
								on D.UploadedBy = AU.Id
								where D.AccountId = @AccountId 
								AND ISNULL(D.IsDeleted,0) = 0
								And (@UploadedBy = 0 or  D.UploadedBy = @UploadedBy)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(D.Name) like '%'+lower(@Search)+'%') OR
										(lower(D.Type) like '%'+lower(@Search)+'%') OR
										(lower(D.Tags) like '%'+lower(@Search)+'%')
									)
								)
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				D.Id,
				D.AccountId,
				D.Name,
				D.OriginalDocumentUrl,
				D.PdfDocumentUrl,
				D.Hardware,
				D.SerialNumber,
				D.DocumentType,
				D.Type,
				D.Tags,
				D.UploadedDate,
				D.UploadedBy,
				AU.FirstName + ' ' + LastName AS FullName,
				AU.Email
			FROM [dbo].[DocListing] D
			left join [AspNetUsers] AU
			on D.UploadedBy = AU.Id
			where D.AccountId = @AccountId 
			AND ISNULL(D.IsDeleted,0) = 0
			And (@UploadedBy = 0 or  D.UploadedBy = @UploadedBy)
			and
			(
				isnull(@Search,'') = '' or
				(
					(lower(D.Name) like '%'+lower(@Search)+'%') OR
					(lower(D.Type) like '%'+lower(@Search)+'%') OR
					(lower(D.Tags) like '%'+lower(@Search)+'%')
				)
			)
			order by D.Id 
			desc
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO


