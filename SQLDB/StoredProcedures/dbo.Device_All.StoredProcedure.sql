/****** Object:  StoredProcedure [dbo].[Device_All]    Script Date: 18-02-2021 02:54:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Device_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Device_All]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- AND DEV.LocationId = ISNULL(NULLIF(@LocationID,0),DEV.LocationId)
-- exec Device_All 0,0,'',2,1142,1,0
CREATE PROCEDURE [dbo].[Device_All]
	@Offset INT = 0,
	@Limit INT = 0,
	@Search VARCHAR(MAX)='',
	@AccountId BIGINT,
	@UserId BIGINT,
	@IsActive BIT = NULL,
	@LocationID BIGINT = 0
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TotalRecords bigint,
	        @IsAdmin bit

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

	IF @IsAdmin IS NULL
	BEGIN
	     SET @AccountId = -1
	END
    
	SET @TotalRecords = (
	                      SELECT 
						         count(*)
						  FROM
						        Device DEV
						   LEFT JOIN
								[Location] LOC ON DEV.LocationId = LOC.Id
						   WHERE
						        DEV.AccountId=@AccountId 
								AND
								DEV.IsActive=ISNULL(@IsActive,DEV.IsActive)
								AND
								(@LocationID=0 OR DEV.LocationId = @LocationID)
		                        AND
		                        (@IsAdmin=1 OR (@IsAdmin = 0 AND DEV.LocationId IN (SELECT ULM.LocationId FROM UserLocationMap ULM WHERE ULM.UserId=@UserId AND IsDeleted=0)))
								AND
								(                         
                                   ISNULL(@Search,'') = '' OR
                                   (
                                     LOWER(DEV.[Name]) LIKE '%'+LOWER(@Search)+'%'
			                         OR LOWER(DEV.SerialNumber) LIKE '%'+LOWER(@Search)+'%'
                                     OR LOWER(LOC.[Name]) LIKE '%'+LOWER(@Search)+'%'
                                   )
								)
	                    )

	IF @Limit = 0
	BEGIN
			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN	
				SET @Limit = 10
			END
	END

	SELECT
	      DEV.Id
         ,DEV.AccountId
         ,DEV.SerialNumber
         ,DEV.DeviceModelId
         ,DEV.FirmwareVersion
         ,DEV.[Name]
         ,DEV.[Description]
         ,DEV.LocationId
         ,DEV.PredecessorSerialNumber
         ,DEV.PredecessorDate
         ,DEV.LastNistSync
         ,DEV.IsActive
         ,DEV.LoggingInterval
         ,DEV.Altitude
         ,DEV.HasLostConnectivity
         ,DEV.CreatedAt
         ,DEV.CreatedBy
         ,DEV.ModifiedAt
         ,DEV.ModifiedBy
         ,DEV.LastLowBatteryAlarm
         ,DEV.Timezone
         ,DEV.Battery
         ,DEV.Wifi
         ,DEV.Latest
         ,DEV.LastUpdated
         ,DEV.CO2LoggingInterval
         ,DEV.IsAlarmRepostEnabled
         ,DEV.AlarmRepostInterval
		 ,LOC.[Name] as LocationName
		 --,(SELECT 
		 --         '['+STRING_AGG([value],',')+']' as dat 
		 --  FROM 
		 --         OPENJSON((
			--	  SELECT *,
			--	         (SELECT CS.[Min] FROM  ChannelSpecification CS WHERE CS.DeviceModelId=DEV.DeviceModelId AND CS.ChannelName = DS.ChannelName) AS ChannelSpecificationMin, 
			--			 (SELECT CS.[Max] FROM  ChannelSpecification CS WHERE CS.DeviceModelId=DEV.DeviceModelId AND CS.ChannelName = DS.ChannelName) AS ChannelSpecificationMax 
			--			 FROM 
			--			 DeviceSetting DS 
			--			 where 
			--			 DS.SerialNumber=DEV.SerialNumber 
			--			 AND 
			--			 DS.AccountId=@AccountId 
			--			 order by DS.ChannelName ASC FOR JSON PATH ))) as DeviceSettingsJson
			,(SELECT 
		          (
				  SELECT *,
				         (SELECT CS.[Min] FROM  ChannelSpecification CS WHERE CS.DeviceModelId=DEV.DeviceModelId AND CS.ChannelName = DS.ChannelName) AS ChannelSpecificationMin, 
						 (SELECT CS.[Max] FROM  ChannelSpecification CS WHERE CS.DeviceModelId=DEV.DeviceModelId AND CS.ChannelName = DS.ChannelName) AS ChannelSpecificationMax 
						 FROM 
						 DeviceSetting DS 
						 where 
						 DS.SerialNumber=DEV.SerialNumber 
						 AND 
						 DS.AccountId=@AccountId 
						 order by DS.ChannelName ASC FOR JSON PATH ) as DeviceSettingsJson) as DeviceSettingsJson
         
		 --,(SELECT '['+[value]+']' FROM OPENJSON((SELECT * FROM DeviceModel where Id=DEV.DeviceModelId FOR JSON PATH))) as DeviceModelJson
		 ,(SELECT (SELECT * FROM DeviceModel where Id=DEV.DeviceModelId FOR JSON PATH)as DeviceModelJson) as DeviceModelJson
		 ,ISNULL(du.IsPined,0) as IsPined
		 ,(select top 1 DeviceThumbnailImage from DeviceModelDetails where DeviceModelId = DEV.DeviceModelId) as DeviceThumbnailImage
	FROM
	     Device DEV
    LEFT JOIN
	     [Location] LOC ON DEV.LocationId = LOC.Id
	LEFT JOIN
		 DeviceUsers du on du.DeviceId = dev.Id AND du.UserId = @UserId
	WHERE
	     DEV.AccountId=@AccountId 
		 AND
		 DEV.IsActive=ISNULL(@IsActive,DEV.IsActive)		
		 AND
		 (@LocationID =0 OR DEV.LocationId = @LocationID)
		 AND
		 (@IsAdmin=1 OR (@IsAdmin = 0 AND DEV.LocationId IN (SELECT ULM.LocationId FROM UserLocationMap ULM WHERE ULM.UserId=@UserId AND IsDeleted=0)))
		 AND 
		 (                         
            ISNULL(@Search,'') = '' OR
            (
              LOWER(DEV.[Name]) LIKE '%'+LOWER(@Search)+'%'
			  OR LOWER(DEV.SerialNumber) LIKE '%'+LOWER(@Search)+'%'
              OR LOWER(LOC.[Name]) LIKE '%'+LOWER(@Search)+'%'
            )
         )
		order by IsPined desc, DEV.Id
		DESC
		OFFSET @offset rows fetch next @Limit rows only

		SELECT @TotalRecords as TotalRecords


		-- fetch the user preferences for this user
	SELECT
	        UP.Id
		   ,UP.UserId
		   ,UP.DataTypeName
		   ,UP.UoMId
		   ,UP.CreatedAt
		   ,UP.CreatedBy
		   ,UP.ModifiedBy
		   ,UP.ModifiedAt
		   ,UOM.UnitName
		   ,UOM.UnitLabel
		   ,UOM.Resolution
	FROM   
	      UserPreference UP
	LEFT JOIN 
	      UnitOfMeasure UOM
	ON    UP.UoMId = UOM.Id
	WHERE 
	      UP.UserId = @UserId

END
GO


