/****** Object:  StoredProcedure [dbo].[AspNetUsers_VerifyUsernameEmail]    Script Date: 12-02-2021 07:23:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[AspNetUsers_VerifyUsernameEmail]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AspNetUsers_VerifyUsernameEmail]
GO

-- =============================================
-- Author:	devansh@rushkar.com
-- Create date: 03-Dec-2020
-- Description:	For checking if the username is correct or not.
-- =============================================

-- select top 3 * from AspNetUsers order by Id desc
-- exec AspNetUsers_VerifyUsernameEmail 'd111@rushkar.com',2,1548
-- update AspNetUsers set PasswordLastUpdated=null,LockoutEndDateUtc=null,LockoutEnd=null where Id=1547
-- delete from AspNetUsers where Id=1547
CREATE PROCEDURE [dbo].[AspNetUsers_VerifyUsernameEmail]
 @Value VARCHAR(256) = null,
 @Type INT = 1, -- Type = 2 means check username | Type = 1 means check email
 @UserId BIGINT = 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Code int = 400,
		    @Message varchar(500) = 'User not found !',
		    @Id int = 0 
    
	IF @Value is null or @Value = ''
    BEGIN
		  SET @Id = 0
		  SET @Code = 400

		  IF @Type = 2
		  BEGIN
		       SET @Message = 'Username is required'
		  END
		  ELSE IF @Type = 1
		  BEGIN
		  	    SET @Message = 'Email is required'
		  END
	END
	ELSE
	BEGIN
	     SELECT 
		       @Id = Id 
	     FROM 
		       AspNetUsers 
	     WHERE 
		       (
			    (@Type = 2 AND LOWER(TRIM(UserName)) = LOWER(TRIM(@Value)))
				OR 
				(@Type = 1 AND LOWER(TRIM(Email)) = LOWER(TRIM(@Value)) AND Id != @UserId)
			   )
		    --   AND
			   --([IsDeactivated] = 0)
			   --AND 
			   --LockoutEnabled = 0
         
		 IF @Id > 0
		 BEGIN
		       SET @Code = 200
		       SET @Message = 'User''s Details verfied !'
		 END
	END

	SELECT @Code as Code,
		   @Message as [Message]

    SELECT 
	       [Id]
          ,[Email]
          ,[EmailConfirmed]
          ,[PasswordHash]
          ,[SecurityStamp]
          ,[CountryCode]
          ,[PhoneNumber]
          ,[PhoneNumberConfirmed]
          ,[TwoFactorEnabled]
          ,[LockoutEndDateUtc]
          ,[LockoutEnabled]
          ,[AccessFailedCount]
          ,[UserName]
          ,[IsAdmin]
          ,[IsSMSEnabled]
          ,[IsEmailEnabled]
          ,[IsPushEnabled]
          ,[AccountId]
          ,[FirstName]
          ,[LastName]
          ,[IsDeactivated]
          ,[Country]
          ,[PasswordLastUpdated]
		  --,[PasswordUpdatedBy]
          ,[ConcurrencyStamp]
          ,[LockoutEnd]
          ,[NormalizedEmail]
          ,[NormalizedUserName]
          --,[CreatedDate]
          --,[CreatedBy]
          --,[UpdatedDate]
          --,[UpdatedBy]
          --,[DeletedDate]
          --,[DeletedBy]
	FROM 
	       AspNetUsers
	WHERE 
	       Id=@Id
END
GO


