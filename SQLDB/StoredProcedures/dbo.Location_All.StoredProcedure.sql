/****** Object:  StoredProcedure [dbo].[Location_All]    Script Date: 12-02-2021 06:54:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Location_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Location_All]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec Location_All 'string',0,10,1015,1580,1,7
CREATE PROCEDURE [dbo].[Location_All]
	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT,
	@AccountId BIGINT = 0,
	@UserId BIGINT = 0,
	@Type INT = 0,
	@Id BIGINT = 0
AS
BEGIN
	
	
    SET NOCOUNT ON;

		DECLARE @TotalRecords bigint,
				@IsAdmin bit;


        SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

		IF @IsAdmin IS NULL OR @AccountId = 0
		BEGIN
		     SET @AccountId = -1
		END
		ELSE IF @IsAdmin = 1
		BEGIN
             SET @UserId = 0
		END


		SET @TotalRecords = (
						      SELECT 
									 count(*)
							  FROM
									 [Location] LOC
							  WHERE					
							  LOC.IsActive=1
							  AND
							  (@AccountId=0 OR LOC.AccountId = @AccountId)
			                  AND
			                  (@UserId=0 OR (@UserId > 0 AND  LOC.Id IN (SELECT LocationId FROM UserLocationMap WHERE UserId=@UserId AND IsDeleted=0)))
							  AND
							  (
							    ISNULL(@Search,'') = ''
							    OR 
							    ((@Type = 1 AND LOWER(LOC.Name) = LOWER(@Search) AND LOC.Id != @Id) OR (@Type = 0 AND LOWER(LOC.Name) LIKE '%'+LOWER(@Search)+'%'))
							  )
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		SELECT 
		       LOC.Id
			  ,LOC.AccountId
			  ,LOC.Name
			  ,LOC.Description
			  ,LOC.Address1
			  ,LOC.Address2
			  ,LOC.City
			  ,LOC.State
			  ,LOC.Zip
			  ,LOC.Country
			  ,LOC.Timezone
			  ,LOC.IanaTimezoneId
			  ,LOC.ParentLocationId
			  ,LOC.CreatedAt
			  ,LOC.CreatedBy
			  ,LOC.ModifiedAt
			  ,LOC.ModifiedBy
			  ,LOC.IsActive
			  ,LOC.IsTransportLocation
			  ,CASE 
			        WHEN @IsAdmin=1 THEN
					     (SELECT COUNT(*) FROM UserLocationMap WHERE LocationId=LOC.Id AND IsDeleted=0) 
					ELSE 0 
			   END as TotalUserCount
			  --,CASE 
			  --      WHEN @IsAdmin=1 THEN
					--     (SELECT COUNT(*) FROM Device DEV WHERE DEV.LocationId=LOC.Id)
					--ELSE 0 
			  -- END as TotalDeviceCount
			   ,(SELECT COUNT(*) FROM Device DEV WHERE DEV.LocationId=LOC.Id) as TotalDeviceCount
		FROM
		      [Location] LOC
		WHERE 
		      LOC.IsActive=1
			  AND
		      (@AccountId=0 OR LOC.AccountId = @AccountId)
			  AND
			  (@UserId = 0 OR (@UserId > 0 AND  LOC.Id IN (SELECT LocationId FROM UserLocationMap WHERE UserId=@UserId AND IsDeleted=0)))
			  AND 
			  (
				 ISNULL(@Search,'') = ''
				 OR 
				 ((@Type = 1 AND LOWER(LOC.Name) = LOWER(@Search) AND LOC.Id != @Id) OR (@Type = 0 AND LOWER(LOC.Name) LIKE '%'+LOWER(@Search)+'%'))
			  )
		order by LOC.Name
		ASC
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
END
GO


