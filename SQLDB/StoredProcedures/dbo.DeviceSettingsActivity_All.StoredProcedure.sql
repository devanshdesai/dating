-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[DeviceSettingsActivity_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeviceSettingsActivity_All]
GO

-- =============================================
-- Author:      Kaushal Patel
-- Create Date: 20-02-2021
-- Description: Used to get all device setting activity details
-- =============================================
CREATE PROCEDURE [dbo].[DeviceSettingsActivity_All]
(
    @Offset int = 0,
	@Limit int = 0,
	@AccountId bigint = 0,
	@SerialNumber nvarchar(50) = NULL,
	@ChannelName nvarchar(25) = NULL,
	@FromDate DateTime = NULL,
	@ToDate DateTime = NULL
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

	if(ISNULL(@ToDate,'') <> '')
	BEGIN
		select @ToDate = Convert(date,@ToDate)
		Select @ToDate = dateadd(hh,23,@ToDate)
		Select @ToDate = dateadd(mi,59,@ToDate)
		Select @ToDate = dateadd(ss,59,@ToDate)
	END

	DECLARE @TotalRecords bigint;

	SET @TotalRecords = (
							select count(*) from [dbo].[DeviceSettingsActivity] A with(nolock)
							where A.CreatedDate >= ISNULL(NULLIF(@FromDate,''),A.CreatedDate)  AND A.CreatedDate <= ISNULL(NULLIF(@ToDate,''),A.CreatedDate)
							AND A.AccountID = ISNULL(NULLIF(@AccountId,0), A.AccountID)
							AND A.SerialNumber = @SerialNumber
							AND A.ChannelName = ISNULL(NULLIF(@ChannelName,''),A.ChannelName)
						)

	IF @Limit = 0
	BEGIN
		SET @Limit = @TotalRecords
		IF @Limit = 0
		BEGIN
				SET @Limit = 10
		END
	END

    select 
		dsa.Id,
		dsa.DeviceSettingId,
		dsa.AccountId,
		dsa.SerialNumber,
		dsa.ChannelName,
		dsa.Alias,
		dsa.Description + ' - ' + u.Email  as Description,
		dsa.IpAddress,
		dsa.Browser,
		dsa.OS,
		dsa.CreatedBy,
		dsa.CreatedDate,
		u.Email as CreatedByEmail
	from DeviceSettingsActivity dsa
	left join AspNetUsers u on u.Id = dsa.CreatedBy
	where dsa.CreatedDate >= ISNULL(NULLIF(@FromDate,''),dsa.CreatedDate)  AND dsa.CreatedDate <= ISNULL(NULLIF(@ToDate,''),dsa.CreatedDate)
	AND dsa.AccountID = ISNULL(NULLIF(@AccountId,0), dsa.AccountID)
	AND dsa.SerialNumber = @SerialNumber
	AND dsa.ChannelName = ISNULL(NULLIF(@ChannelName,''),dsa.ChannelName)
	Order by dsa.Id desc
	OFFSET @offset rows fetch next @Limit rows only

	select @TotalRecords as TotalRecords
END
GO
