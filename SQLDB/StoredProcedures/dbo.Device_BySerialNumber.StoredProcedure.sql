/****** Object:  StoredProcedure [dbo].[Device_BySerialNumber]    Script Date: 12-02-2021 08:33:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Device_BySerialNumber]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Device_BySerialNumber]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec Device_BySerialNumber 2,1142,'100000010'
-- exec Device_BySerialNumber 2,1142,'100000005'
CREATE PROCEDURE [dbo].[Device_BySerialNumber] 
@AccountId BIGINT,
@UserId BIGINT,
@SerialNumber NVARCHAR(50)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 200,
	        @Message VARCHAR(250) = '',
			@IsAdmin bit,
			@Id BIGINT = 0,
			@DeviceModelId BIGINT = 0;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

	IF @IsAdmin IS NULL
	BEGIN
	     SET @Code = 400
		 SET @Message = 'Not able to access the device details'
		 SET @Id = 0
		 SET @SerialNumber = ''
	END
	ELSE
	BEGIN
	     
		 SELECT 
		       @Id=Id,
			   @DeviceModelId=DeviceModelId
	     FROM 
		       Device 
	     WHERE 
		       AccountId=@AccountId 
			   AND SerialNumber=LOWER(@SerialNumber) 
			   AND (@IsAdmin=1 OR (@IsAdmin = 0 AND LocationId IN (SELECT ULM.LocationId FROM UserLocationMap ULM WHERE ULM.UserId=@UserId AND IsDeleted=0)))
	     
	     IF @Id > 0
		 BEGIN
			   	SET @Code = 200
				SET @Message = 'Device details'
		 END
		 ELSE
		 BEGIN
		       SET @Code = 400
			   SET @Message = 'Device details not found'
			   SET @Id = 0
			   SET @SerialNumber = ''
		 END
	END

	SELECT @Code as Code,
		   @Message as [Message]

    
	-- select device details	    
	SELECT 
	       Id
          ,AccountId
          ,SerialNumber
          ,FirmwareVersion
          ,[Name]
          ,[Description]
          ,LocationId
          ,PredecessorSerialNumber
          ,PredecessorDate
          ,LastNistSync
          ,IsActive
          ,LoggingInterval
          ,Altitude
          ,HasLostConnectivity
          ,CreatedAt
          ,CreatedBy
          ,ModifiedAt
          ,ModifiedBy
          ,LastLowBatteryAlarm
          ,Timezone
          ,Battery
          ,Wifi
          ,Latest
          ,LastUpdated
          ,CO2LoggingInterval
          ,IsAlarmRepostEnabled
          ,AlarmRepostInterval
		  ,(select top 1 DeviceThumbnailImage from DeviceModelDetails where DeviceModelId = Device.DeviceModelId) as DeviceThumbnailImage
	FROM
	      Device
	WHERE 
	      Id=@Id

	-- select device settings details
	SELECT 
	       DS.Id
          ,DS.AccountId
          ,DS.SerialNumber
          ,DS.Channel
          ,DS.ChannelName
          ,DS.Alias
          ,DS.DataType
          ,DS.[Min]
          ,DS.[Max]
          ,DS.SentToDevice
          ,DS.IsDeactived
          ,DS.CreatedBy
          ,DS.CreatedAt
          ,DS.ModifiedBy
          ,DS.ModifiedAt
          ,DS.DeviceSerialNumber
		  ,(SELECT [Min] FROM  ChannelSpecification CS WHERE CS.DeviceModelId=@DeviceModelId AND CS.ChannelName = DS.ChannelName) AS ChannelSpecificationMin
		  ,(SELECT [Max] FROM  ChannelSpecification CS WHERE CS.DeviceModelId=@DeviceModelId AND CS.ChannelName = DS.ChannelName) AS ChannelSpecificationMax
	FROM
	       DeviceSetting DS
	WHERE 
	       DS.SerialNumber=@SerialNumber
		   AND
		   DS.AccountId = @AccountId
		   AND
		   DS.IsDeactived = 0
	ORDER BY
	       DS.ChannelName ASC
	       

	-- select device model 
	SELECT 
	       Id
          ,Model
          ,[Name]
          ,DeviceType
          ,MaxBattery
          ,MinBattery
          ,LowBattery
          ,isEthernetDevice
          ,isMqtt
          ,IsEthernetEnabled
          ,IsBTHub
	FROM
	       DeviceModel
	WHERE 
	       Id=@DeviceModelId

	-- select channel Specification for that model
	--SELECT 
	--       Id
 --          ,ChannelName
 --          ,DataType
 --          ,[Max]
 --          ,[Min]
 --          ,DeviceModelId
	--FROM
	--      ChannelSpecification
	--WHERE 
	--      DeviceModelId=@DeviceModelId
	--ORDER BY
	--       ChannelName ASC
    
	-- fetch the user preferences for this user
	SELECT
	        UP.Id
		   ,UP.UserId
		   ,UP.DataTypeName
		   ,UP.UoMId
		   ,UP.CreatedAt
		   ,UP.CreatedBy
		   ,UP.ModifiedBy
		   ,UP.ModifiedAt
		   ,UOM.UnitName
		   ,UOM.UnitLabel
		   ,UOM.Resolution
	FROM   
	      UserPreference UP
	LEFT JOIN 
	      UnitOfMeasure UOM
	ON    UP.UoMId = UOM.Id
	WHERE 
	      UP.UserId = @UserId


	SELECT
	       Id,
		   UserId,
		   DeviceId,
		   CouponId,
		   CouponCode,
		   AppliedDate,
		   ExpiryDate,
		   ChannelPartnerName
    FROM
	      DeviceCoupon
	WHERE 
	      DeviceId = @Id


    SELECT 
	       [Id]
		  ,[AccountId]
		  ,[Name]
		  ,[Description]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[Country]
		  ,[Timezone]
		  ,[IanaTimezoneId]
		  ,[ParentLocationId]
		  ,[CreatedAt]
		  ,[CreatedBy]
		  ,[ModifiedAt]
		  ,[ModifiedBy]
		  ,[IsActive]
		  ,[IsTransportLocation]
		  ,(SELECT TOP 1 LI.LocationPhoto FROM LocationImages LI WHERE LI.LocationId in (SELECT D.LocationId FROM Device D WHERE D.Id=@Id) ORDER BY LI.Id Desc) as LocationPhoto 
	FROM
	       [Location]
    WHERE
	       Id in (SELECT LocationId FROM Device WHERE Id=@Id)
		   AND
		   IsActive = 1


    -- fetch the device model details

	SELECT 
		  * 
    FROM
	      DeviceModelDetails 
	WHERE 
		  DeviceModelId=@DeviceModelId

END
GO


