/****** Object:  StoredProcedure [dbo].[AspNetUsers_ById]    Script Date: 12-02-2021 07:27:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[AspNetUsers_ById]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AspNetUsers_ById]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AspNetUsers_ById] 
@Id BIGINT,
@AccountId BIGINT,
@UserId BIGINT
AS
BEGIN

	SET NOCOUNT ON;
   
    DECLARE @Code INT = 200,
	        @Message VARCHAR(250) = 'Data Found',
			@IsAdmin bit;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

	IF @IsAdmin IS NULL OR (@IsAdmin = 0 AND @Id != @UserId)
	BEGIN
	     SET @Code = 400
		 SET @Message = 'User Not Found'
		 SET @Id=0
	END
	ELSE
	BEGIN
		 IF (select count(*) from AspNetUsers where Id = @Id AND AccountId = @AccountId AND IsDeactivated=0) > 0
		 BEGIN
		       SET @Code = 200
			  SET @Message = 'User Found'
		 END
		 ELSE
		 BEGIN
		       SET @Code = 400
		       SET @Message = 'User Not Found'
		       SET @Id=0
		 END
	END


	SELECT @Code as Code,
		   @Message as [Message]
	
	SELECT
	       ANP.Id
          ,ANP.Email
          ,ANP.CountryCode
          ,ANP.PhoneNumber
          ,ANP.LockoutEnabled
          ,ANP.IsAdmin
          ,ANP.IsSMSEnabled
          ,ANP.IsEmailEnabled
          ,ANP.IsPushEnabled
          ,ANP.AccountId
          ,ANP.FirstName
          ,ANP.LastName
		  ,ANP.Country
		  ,(SELECT CompanyName FROM Account WHERE Id=@AccountId) as CompanyName
		  ,CASE 
		       WHEN (SELECT count(*) FROM EscalationData WHERE (UserIds = CONVERT(VARCHAR,ANP.Id) OR UserIds LIKE CONVERT(VARCHAR,ANP.Id)+ ',%' OR UserIds LIKE '%,' + CONVERT(VARCHAR,ANP.Id) + ',%' OR UserIds LIKE '%,' + CONVERT(VARCHAR,ANP.Id)) AND IsDeleted=0) > 0
			   THEN 1
			   ELSE 0
		   END  AS IsInEscalation
		 ,(select Top 1 [Timestamp]  from LoginHistories WHERE UserId=@Id AND IsSuccess=1 order by Id desc) as LastLogin
		 ,CASE WHEN ISNULL(ANP.PhoneNumber,'') <> '' 
		  THEN (select top 1 IsVerified from AspNetUsersPhoneNumbers Where AspNetUserId=@Id order by Id desc)
		  ELSE 1
		  END as IsSMSVerified
		 ,CASE WHEN ISNULL(ANP.Email,'') <> '' 
		  THEN (select top 1 IsVerified from AspNetUsersEmails Where AspNetUserId=@Id order by Id desc)
		  ELSE 1
		  END as IsEmailVerified
    FROM
	      AspNetUsers ANP
	WHERE 
	      ANP.Id = @Id
		  AND
		  ANP.AccountId = @AccountId
		  AND 
		  ANP.IsDeactivated=0 
		  

    -- fetch the locations for this user
	SELECT 
	       LocationId
	FROM
	       UserLocationMap 
    WHERE
	       UserId = @Id 
		   AND
		   IsDeleted = 0

	-- fetch the user preferences for this user
	SELECT
	        UP.Id
		   ,UP.UserId
		   ,UP.DataTypeName
		   ,UP.UoMId
		   ,UP.CreatedAt
		   ,UP.CreatedBy
		   ,UP.ModifiedBy
		   ,UP.ModifiedAt
		   ,UOM.UnitName
		   ,UOM.UnitLabel
		   ,UOM.Resolution
	FROM   
	      UserPreference UP
	LEFT JOIN 
	      UnitOfMeasure UOM
	ON    UP.UoMId = UOM.Id
	WHERE 
	      UP.UserId = @Id

	-- fetch the account details for this user
	SELECT
	      Id
         ,CompanyName
         ,Address1
         ,Address2
         ,City
         ,State
         ,Zip
         ,Country
         ,CountryCode
         ,Phone
         ,SubscriptionStart
         ,IsTrialSubscription
         ,IsActive
         ,IsSMSEnabled
         ,IsMonthlySubscription
         ,IsAnnualSubscription
         ,IsInvoiceAccount
         ,HasPaymentMethod
         ,PartnerId
         ,CreatedAt
         ,CreatedBy
         ,ModifiedAt
         ,ModifiedBy
         ,IsSuspended
         ,IsTraceableGoEnabled
         ,IsPremium
         ,ServiceLevel
	FROM 
	     Account
	WHERE 
	     Id=@AccountId
END
GO


