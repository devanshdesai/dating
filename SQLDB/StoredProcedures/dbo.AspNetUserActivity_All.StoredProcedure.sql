/****** Object:  StoredProcedure [dbo].[AspNetUserActivity_All]    Script Date: 12-02-2021 07:30:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[AspNetUserActivity_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AspNetUserActivity_All]
GO

--exec [AspNetUserActivity_All] '',0,0,0
CREATE PROCEDURE [dbo].[AspNetUserActivity_All]
	@Search varchar(MAX),
	@Offset int,
	@Limit int,
	@UserId bigint = 0,
	@FromDate DateTime = NULL,
	@ToDate DateTime = NULL
AS
BEGIN

		SET NOCOUNT ON;

		if(ISNULL(@ToDate,'') <> '')
		BEGIN
			select @ToDate = Convert(date,@ToDate)
			Select @ToDate = dateadd(hh,23,@ToDate)
			Select @ToDate = dateadd(mi,59,@ToDate)
			Select @ToDate = dateadd(ss,59,@ToDate)
		END

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[AspNetUserActivity] A
								
								--left join [UserPreference] U
								--on A.UserId = U.Id
								
								where (@UserId = 0 or  A.UserId = @UserId)
								AND A.CreatedDate >= ISNULL(NULLIF(@FromDate,''),A.CreatedDate)  AND A.CreatedDate <= ISNULL(NULLIF(@ToDate,''),A.CreatedDate)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(A.UserId) like '%'+lower(@Search)+'%')
									)
								)
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				A.Id,
				A.UserId,
				A.Activity,
				A.IpAddress,
				A.BearerToken,
				A.CreatedDate

			FROM [dbo].[AspNetUserActivity] A
			
			--left join [UserPreference] U
			--on A.UserId = U.Id
			
			where (@UserId = 0 or  A.UserId = @UserId)
			AND A.CreatedDate >= ISNULL(NULLIF(@FromDate,''),A.CreatedDate)  AND A.CreatedDate <= ISNULL(NULLIF(@ToDate,''),A.CreatedDate)
			and
			(
				isnull(@Search,'') = '' or
				(
					(lower(A.UserId) like '%'+lower(@Search)+'%')
				)
			)
			order by A.Id 
			
			desc
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO


