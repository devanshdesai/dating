/****** Object:  StoredProcedure [dbo].[Device_Log_All]    Script Date: 12-02-2021 08:41:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Device_Log_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Device_Log_All]
GO

CREATE PROCEDURE [dbo].[Device_Log_All]
	@Search varchar(MAX) = '',
	@Offset int = 0,
	@Limit int = 0,
	@UserId bigint = 0,
	@AccountId bigint = 0,
	@ActionType varchar(50) = NULL,
	@FromDate DateTime = NULL,
	@ToDate DateTime = NULL
AS
BEGIN

		SET NOCOUNT ON;

		if(ISNULL(@ToDate,'') <> '')
		BEGIN
			select @ToDate = Convert(date,@ToDate)
			Select @ToDate = dateadd(hh,23,@ToDate)
			Select @ToDate = dateadd(mi,59,@ToDate)
			Select @ToDate = dateadd(ss,59,@ToDate)
		END

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[Device_Log] L with(nolock)
								where (@UserId = 0 or  L.ActionBy = @UserId)
								AND L.ActionDate >= ISNULL(NULLIF(@FromDate,''),L.ActionDate)  AND L.ActionDate <= ISNULL(NULLIF(@ToDate,''),L.ActionDate)
								AND L.ActionType = ISNULL(NULLIF(@ActionType,''), L.ActionType)
								AND L.AccountID = ISNULL(NULLIF(@AccountId,0), L.AccountID)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(L.ActionBy) like '%'+lower(@Search)+'%')
									)
								)
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				Id
				DeviceId,
				AccountId,
				SerialNumber,
				DeviceKey,
				DeviceModelId,
				FirmwareVersion,
				[Name],
				[Description],
				LocationId,
				PredecessorSerialNumber,
				PredecessorDate,
				LastNistSync,
				IsActive,
				LoggingInterval,
				Altitude,
				HasLostConnectivity,
				CreatedAt,
				CreatedBy,
				ModifiedAt,
				ModifiedBy,
				LastLowBatteryAlarm,
				Timezone,
				Battery,
				Wifi,
				Latest,
				LastUpdated,
				CO2LoggingInterval,
				IsAlarmRepostEnabled,
				AlarmRepostInterval,
				ActionType,
				ActionDate,
				ActionBy,
				ActionByEmail,
				IpAddress,
				Browser,
				OS
			FROM [dbo].[Device_Log] L with(nolock)
			where (@UserId = 0 or  L.ActionBy = @UserId)
			AND L.ActionDate >= ISNULL(NULLIF(@FromDate,''),L.ActionDate)  AND L.ActionDate <= ISNULL(NULLIF(@ToDate,''),L.ActionDate)
			AND L.ActionType = ISNULL(NULLIF(@ActionType,''), L.ActionType)
			AND L.AccountID = ISNULL(NULLIF(@AccountId,0), L.AccountID)
			and
			(
				isnull(@Search,'') = '' or
				(
					(lower(L.ActionBy) like '%'+lower(@Search)+'%')
				)
			)
			order by L.Id 
			
			desc
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
END
GO


