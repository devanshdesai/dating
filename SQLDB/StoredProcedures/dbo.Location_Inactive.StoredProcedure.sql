/****** Object:  StoredProcedure [dbo].[Location_Inactive]    Script Date: 12-02-2021 07:06:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Location_Inactive]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Location_Inactive]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Location_Inactive]
@Id BIGINT = 0,
@AccountId BIGINT = 0,
@UserId BIGINT = 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Code INT = 200,
	        @Message VARCHAR(250) = 'Data Retrived',
			@IsAdmin bit;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers 
	WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0 


	IF @IsAdmin IS NULL OR @IsAdmin = 0 OR @AccountId = 0 
    BEGIN
         SET @Code = 400
		 SET @Message = 'Unauthorized request'
		 SET @Id=0
    END
	ELSE
	BEGIN
	     IF (SELECT COUNT(*) FROM [Location] WHERE Id=@Id AND IsActive=1 AND AccountId=@AccountId) > 0
		 BEGIN
		      	--IF (SELECT COUNT(*) FROM UserLocationMap WHERE LocationId = @Id and IsDeleted=0) > 0 
		       -- BEGIN
		       --       SET @Code = 400
		  	    --      SET @Message = 'Location is being used by the used'
		       -- END
			   IF (SELECT COUNT(*) FROM Device where AccountId=@AccountId AND LocationId=@Id) > 0
			   BEGIN
			         SET @Code = 400
		             SET @Message = 'Device/s is located at this location. Please remove the location from the device first to delete the location'
		             SET @Id=0
			   END
			   ELSE IF (SELECT COUNT(*) FROM Device where AccountId=@AccountId AND LocationId=@Id AND SerialNumber in (SELECT SerialNumber FROM EscalationData WHERE AccountId=@AccountId AND IsDeleted=0)) > 0
			   BEGIN
			         SET @Code = 400
		             SET @Message = 'Device/s at this location has escalation channels. Please remove the escalation channels first to delete the location'
		             SET @Id=0
			   END
			   ELSE
			   BEGIN
			         UPDATE
				           [Location]
				     SET 
				            IsActive=0
				     WHERE 
				            Id=@Id

				     UPDATE 
				            [UserLocationMap]
				     SET 
				            IsDeleted=1
				     WHERE
				            LocationId=@Id
              
		        	UPDATE 
		        		   [NotificationSettings]
		        	SET 
		        	       IsDeleted=1
		        	WHERE
		        		   UserLocationId IN (SELECT Id FROM [UserLocationMap] WHERE LocationId=@Id AND IsDeleted=1)


				    SET @Code = 200
				    SET @Message = 'Location Deleted Successfully'
			   END

		 END
		 ELSE
		 BEGIN
		      SET @Code = 400
		  	  SET @Message = 'Location Not Found'
			  SET @Id=0
		 END
	END

	SELECT @Code as Code,
		   @Message as [Message]

	 --SET @Id = 0

	 SELECT * FROM [Location] WHERE Id=@Id
END
GO


