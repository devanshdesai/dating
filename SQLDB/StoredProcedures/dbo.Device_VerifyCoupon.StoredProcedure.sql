/****** Object:  StoredProcedure [dbo].[Device_VerifyCoupon]    Script Date: 12-02-2021 08:37:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Device_VerifyCoupon]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Device_VerifyCoupon]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Device_VerifyCoupon]
@AccountId BIGINT,
@UserId BIGINT,
@CouponCode varchar(250)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(250) = '',
			@IsAdmin bit,
			@CouponId BIGINT = 0;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 AND AccountId=@AccountId --AND LockoutEnabled=0 

	-- check if an admin is accessing
	IF @IsAdmin IS NULL OR @IsAdmin = 0 
	BEGIN
	     SET @Code = 400
		 SET @Message = 'Not allowed to add a device'
	END
	ELSE
	BEGIN
	     SELECT @CouponId=Id FROM LookupCoupons WHERE TRIM(LOWER(CouponCode)) = TRIM(LOWER(@CouponCode)) AND RedeemedDate IS NULL
	     IF @CouponId > 0
		 BEGIN
		      SET @Code = 200
			  SET @Message = 'Coupon Code Found'
		 END
		 ELSE
		 BEGIN
		      SET @Code = 400
			  SET @Message = 'Coupon code is incorrect'
			  SET @CouponId = 0
		 END
	END

	SELECT @Code as Code,
		   @Message as [Message]

	SELECT
		 Id
		,CouponCode
		,[Description]
		,ValidForDays
		,Distributor
		,RedeemedDate
		,ServiceType
	FROM
	     LookupCoupons
    WHERE
	     Id=@CouponId
    
END
GO


