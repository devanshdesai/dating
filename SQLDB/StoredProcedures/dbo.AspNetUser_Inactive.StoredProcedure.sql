/****** Object:  StoredProcedure [dbo].[AspNetUser_Inactive]    Script Date: 12-02-2021 07:28:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[AspNetUser_Inactive]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AspNetUser_Inactive]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AspNetUser_Inactive]
@Id BIGINT = 0,
@AccountId BIGINT = 0,
@UserId BIGINT = 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Code INT = 200,
	        @Message VARCHAR(250) = 'User In-Activated',
			@IsAdmin bit;

    SELECT @IsAdmin = IsAdmin FROM AspNetUsers 
	WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0
	
	IF @IsAdmin IS NULL OR @IsAdmin = 0
	BEGIN
	     SET @Code = 400
		 SET @Message = 'Unauthorized request'
		 SET @Id=0
	END
	ELSE
	BEGIN
	     IF (SELECT COUNT(*) FROM [AspNetUsers] WHERE Id=@Id AND IsDeactivated=0 AND AccountId=@AccountId) > 0
		 BEGIN

		      IF (SELECT count(*) FROM EscalationData WHERE (UserIds = CONVERT(VARCHAR,@Id) OR UserIds LIKE CONVERT(VARCHAR,@Id)+ ',%' OR UserIds LIKE '%,' + CONVERT(VARCHAR,@Id) + ',%' OR UserIds LIKE '%,' + CONVERT(VARCHAR,@Id)) AND IsDeleted=0) > 0
			  BEGIN
			        SET @Code = 400
			        SET @Message = 'User is a part of escalation channel. Please remove the user from the escalation channel first.'
			        SET @Id=0
			  END
			  ELSE
			  BEGIN  
			      UPDATE 
			            [AspNetUsers]
			      SET
			            IsDeactivated = 1
			      WHERE 
					    Id=@Id

			      UPDATE 
				         [UserLocationMap]
			      SET 
			             IsDeleted=1
			      WHERE
			  		     UserId=@Id

			      UPDATE 
				        [NotificationSettings]
			      SET 
			             IsDeleted=1
			      WHERE
			     	    UserLocationId IN (SELECT Id FROM [UserLocationMap] WHERE UserId=@Id AND IsDeleted=1)
			  END 
		 END
		 ELSE
		 BEGIN
		     SET @Code = 400
			 SET @Message = 'User Not Found'
			 SET @Id=0
		 END
	END

	SELECT @Code as Code,
		   @Message as [Message]

	 --SET @Id = 0

	 SELECT * FROM [AspNetUsers] WHERE Id=@Id

END
GO


