/****** Object:  StoredProcedure [dbo].[Reports_All]    Script Date: 12-02-2021 08:42:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Reports_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Reports_All]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec Reports_All '',0,0,2,1142
CREATE PROCEDURE [dbo].[Reports_All] 
	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT,
	@AccountId BIGINT = 0,
	@UserId BIGINT = 0
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TotalRecords bigint,
			@IsAdmin bit;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

	SET @TotalRecords = (
						  SELECT 
						         COUNT(*)
						   FROM    
						         Reports
                           WHERE 
                    	   (
							  --Not (@IsAdmin IS NULL OR @IsAdmin = 0)
							  --AND
							  UserID=@UserId 
							  AND 
							  AccountID=@AccountId
							  AND
                    	      (ISNULL(@Search,'') = '' OR
                    	      (
                    	           LOWER(SerialNumbers) LIKE '%'+LOWER(@Search)+'%'
                    	      )
							  )
                    	   )
						)


	IF @Limit = 0
	BEGIN
			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN	
				SET @Limit = 10
			END
	END

	SELECT
	       *
    FROM
	      Reports
	WHERE 
		  (        
		     --Not (@IsAdmin IS NULL OR @IsAdmin = 0)  
			 --AND
			 UserID=@UserId 
			 AND 
			 AccountID=@AccountId				  
			 AND
		     (ISNULL(@Search,'') = '' OR
			  (
		        LOWER(SerialNumbers) LIKE '%'+LOWER(@Search)+'%'
			  )
			 )
		  )
	    order by ID
		Desc
		OFFSET @offset rows fetch next @Limit rows only

	SELECT @TotalRecords as TotalRecords
    
END

GO


