/****** Object:  StoredProcedure [dbo].[DocListing_ById]    Script Date: 19-02-2021 12:48:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[DocListing_ById]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DocListing_ById]
GO

--exec [dbo].[DocListing_ById] 1
CREATE PROCEDURE [dbo].[DocListing_ById]
	@Id bigint
AS
BEGIN
	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
            @Message varchar(200) = ''


			SET @Message = 'DocListing Data Retrived Successfully'

	       SELECT @Code as Code,
				  @Message as [Message]

	select
				D.Id,
				D.AccountId,
				D.Name,
				D.OriginalDocumentUrl,
				D.PdfDocumentUrl,
				D.Hardware,
				D.SerialNumber,
				D.DocumentType,
				D.Type,
				D.Tags,
				D.UploadedDate,
				D.UploadedBy,
				AU.FirstName + ' ' + LastName AS FullName,
				AU.Email

			FROM [dbo].[DocListing] D

			left join [AspNetUsers] AU
			on D.UploadedBy = AU.Id
			
		WHERE
				D.Id = @Id
		
END
GO


