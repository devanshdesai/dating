/****** Object:  StoredProcedure [dbo].[EscalationService_GetPendingData]    Script Date: 12-02-2021 08:45:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[EscalationService_GetPendingData]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EscalationService_GetPendingData]
GO

-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[EscalationService_GetPendingData]  
 -- Add the parameters for the stored procedure here  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
 select es.ID, ed.id as EscalationID, es.FireTime ,STUFF((select ',' + u.Email  
   from dbo.SplitString(ed.userids,',') ui  
   inner join AspNetUsers u on u.ID = ui.item FOR XML PATH ('')), 1, 1, '') as EmailSentTo,  
   STUFF((select ',' + NULLIF(u.PhoneNumber,'')  
   from dbo.SplitString(ed.userids,',') ui  
   inner join AspNetUsers u on u.ID = ui.item FOR XML PATH ('')), 1, 1, '') as SmsSentTo,  
   ed.SerialNumber,  
   ds.Alias as ChannelName,  
   d.Name as Device,  
   l.Name as Location  
 from EscalationService es  
 inner join escalationdata ed on ed.id = es.escalationid  
 left outer join devicesetting ds on ds.ID = ed.DeviceSettingID  
 left outer join device d on d.SerialNumber = ds.SerialNumber AND d.AccountID = ds.AccountID  
 left outer join Location l on l.ID = d.LocationID AND l.AccountID = d.AccountID  
 where ISNULL(es.IsEmailSent,0) = 0  
	AND ISNULL(es.IsSmsSent,0) = 0 
	AND ISNULL(es.IsAlarmAck,0) = 0
    AND es.FireTime <= getdate()  
END  
GO


