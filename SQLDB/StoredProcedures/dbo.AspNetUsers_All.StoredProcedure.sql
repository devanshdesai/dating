/****** Object:  StoredProcedure [dbo].[AspNetUsers_All]    Script Date: 12-02-2021 07:24:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[AspNetUsers_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AspNetUsers_All]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AspNetUsers_All] 
	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT,
	@AccountId BIGINT = 0,
	@UserId BIGINT = 0,
	@UserType INT = 0,
	@LocationId BIGINT = 0
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TotalRecords bigint,
			@IsAdmin bit;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

	IF @IsAdmin IS NULL OR @IsAdmin = 0
	BEGIN
	     SET @AccountId = -1
	END

	SET @TotalRecords = (
						  SELECT 
						         COUNT(*)
						   FROM    
						         AspNetUsers
                           WHERE 
                    	   (@AccountId=0  OR AccountId = @AccountId)	 
                    	   AND
                    	   IsDeactivated=0
						   AND 
						   (@UserType=0 OR (@UserType = 1 AND IsAdmin=1) OR (@UserType = 2 AND IsAdmin=0))
                    	   AND
                    	   (                         
                    	      ISNULL(@Search,'') = '' OR
                    	      (
                    	           LOWER(Email) LIKE '%'+LOWER(@Search)+'%'
                                   OR LOWER(FirstName)+' '+Lower(LastName) LIKE '%'+LOWER(@Search)+'%'
                    	      )
                    	   )
						)


	IF @Limit = 0
	BEGIN
			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN	
				SET @Limit = 10
			END
	END

	SELECT
	       ANP.Id
          ,ANP.Email
          ,ANP.CountryCode
		  ,ANP.Country
          ,ANP.PhoneNumber
          ,Case When ANP.LockoutEndDateUtc is null OR ANP.LockoutEndDateUtc <= GETUTCDATE() THEN 0 ELSE 1 END as LockoutEnabled
          ,ANP.IsAdmin
          ,ANP.IsSMSEnabled
          ,ANP.IsEmailEnabled
          ,ANP.IsPushEnabled
          ,ANP.AccountId
          ,ANP.FirstName
          ,ANP.LastName
          --,ANP.CreatedDate
          --,ANP.CreatedBy
          --,ANP.UpdatedDate
          --,ANP.UpdatedBy
          --,ANP.DeletedDate
          --,ANP.DeletedBy
		  ,(SELECT COUNT(*) FROM UserLocationMap WHERE UserId = ANP.Id and IsDeleted=0) as TotalLocationCount
		  ,CASE 
		       WHEN (SELECT count(*) FROM EscalationData WHERE (UserIds = CONVERT(VARCHAR,ANP.Id) OR UserIds LIKE CONVERT(VARCHAR,ANP.Id)+ ',%' OR UserIds LIKE '%,' + CONVERT(VARCHAR,ANP.Id) + ',%' OR UserIds LIKE '%,' + CONVERT(VARCHAR,ANP.Id)) AND IsDeleted=0) > 0
			   THEN 1
			   ELSE 0
		   END  AS IsInEscalation	
		   ,(select Top 1 [Timestamp]  from LoginHistories WHERE UserId=ANP.Id AND IsSuccess=1 order by Id desc) as LastLogin
		   ,CASE WHEN ISNULL(ANP.PhoneNumber,'') <> '' 
		  THEN (select top 1 IsVerified from AspNetUsersPhoneNumbers Where AspNetUserId=ANP.Id order by Id desc)
		  ELSE 1
		  END as IsSMSVerified
		  ,CASE WHEN ISNULL(ANP.Email,'') <> '' 
		  THEN (select top 1 IsVerified from AspNetUsersEmails Where AspNetUserId=ANP.Id order by Id desc)
		  ELSE 1
		  END as IsEmailVerified
    FROM
	      AspNetUsers ANP
	WHERE 
		  (@AccountId=0 OR ANP.AccountId = @AccountId)	 
	      AND
		  ANP.IsDeactivated=0
		  AND 
		  (@UserType=0 OR (@UserType = 1 AND IsAdmin=1) OR (@UserType = 2 AND IsAdmin=0))
		  AND
		  (@LocationId=0 OR (@LocationId > 0 AND ANP.Id IN (SELECT UserId FROM UserLocationMap WHERE LocationId = @LocationId)))
		  AND
		  (                         
		     ISNULL(@Search,'') = '' OR
			 (
		       LOWER(ANP.Email) LIKE '%'+LOWER(@Search)+'%'
               OR LOWER(ANP.FirstName)+' '+Lower(ANP.LastName) LIKE '%'+LOWER(@Search)+'%'
			 )
		  )
	    order by ANP.FirstName
		ASC
		OFFSET @offset rows fetch next @Limit rows only

	SELECT @TotalRecords as TotalRecords
    
END
GO


