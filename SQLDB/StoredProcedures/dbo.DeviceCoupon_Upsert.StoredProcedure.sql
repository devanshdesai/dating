/****** Object:  StoredProcedure [dbo].[DeviceCoupon_Upsert]    Script Date: 12-02-2021 08:38:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[DeviceCoupon_Upsert]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeviceCoupon_Upsert]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeviceCoupon_Upsert] 
@UserId BIGINT,
@DeviceId BIGINT,
@CouponId BIGINT,
@ChannelPartnerName varchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(250) = '',
			@Id BIGINT = 0,
			@CouponCode VARCHAR(250) = '',
			@ValidForDays INT = 0,
			@AppliedDate DATETIME,
			@ExpiryDate DATETIME;

	SELECT @CouponCode=CouponCode, @ValidForDays=ValidForDays  FROM LookupCoupons WHERE Id=@CouponId

	SET @AppliedDate = GETDATE();
	SET @ExpiryDate = DATEADD(day, @ValidForDays, @AppliedDate)

	INSERT INTO DeviceCoupon
	(UserId,DeviceId,CouponId,CouponCode,AppliedDate,[ExpiryDate],ChannelPartnerName)
	VALUES
	(@UserId,@DeviceId,@CouponId,@CouponCode,@AppliedDate,@ExpiryDate,@ChannelPartnerName)

	SET @Id = SCOPE_IDENTITY()

	UPDATE LookupCoupons SET RedeemedDate=GETDATE() WHERE Id=@CouponId

	SELECT @Code as Code,
		   @Message as [Message]

	SELECT
		  Id,
		  UserId,
		  DeviceId,
		  CouponId,
		  CouponCode,
		  AppliedDate,
		  ExpiryDate,
		  ChannelPartnerName
	FROM
	      DeviceCoupon
	WHERE 
          Id=@Id
END
GO


