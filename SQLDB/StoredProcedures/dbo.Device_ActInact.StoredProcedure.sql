/****** Object:  StoredProcedure [dbo].[Device_ActInact]    Script Date: 12-02-2021 08:34:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Device_ActInact]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Device_ActInact]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Device_ActInact] 
@AccountId BIGINT,
@UserId BIGINT,
@SerialNumber NVARCHAR(50)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 200,
	        @Message VARCHAR(250) = '',
			@IsAdmin bit,
			@Id BIGINT = 0,
			@IsActive BIT;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

	IF @IsAdmin IS NULL OR @IsAdmin = 0
	BEGIN
	     SET @Code = 400
		 SET @Message = 'Not allowed to perform the action'
		 SET @Id=0
	END 
	ELSE
	BEGIN
	      SELECT 
		       @Id=Id,
			   @IsActive=IsActive
	      FROM 
		         Device 
	      WHERE 
		       AccountId=@AccountId 
			   AND SerialNumber=LOWER(@SerialNumber) 
			   AND (@IsAdmin !=0 OR (@IsAdmin = 0 AND LocationId IN (SELECT ULM.LocationId FROM UserLocationMap ULM WHERE ULM.UserId=@UserId AND IsDeleted=0)))

	      IF @Id > 0
		  BEGIN
		        IF @IsActive = 1
				BEGIN
					IF NOT EXISTS (select top 1 1 from EscalationData ed where LOWER(ed.SerialNumber) = LOWER(@SerialNumber) AND ed.AccountId=@AccountId AND ISNULL(ed.IsDeleted,0) = 0)
					BEGIN
				       UPDATE 
					          Device
					   SET   
					          IsActive = 0
					   WHERE 
					          Id=@Id

					   SET @Message = 'Device in-activated successfully'
					   SET @Code = 200
					END
					ELSE
					BEGIN
						SET @Code = 400
						SET @Message = 'Please remove escalation prior to deactivate deivce.'
						SET @Id=0
					END
				END 
				ELSE IF @IsActive = 0
				BEGIN
				       UPDATE 
					          Device
					   SET   
					          IsActive = 1
					   WHERE 
					          Id=@Id

					   SET @Message = 'Device activated successfully'
					   SET @Code = 200
				END
			   
		       --SET @Id=0
		  END
		  ELSE
		  BEGIN
		       SET @Code = 400
		       SET @Message = 'No device found to perform the action'
		       SET @Id=0
		  END
	END

    SELECT @Code as Code,
		   @Message as [Message]

    -- select device details	    
	SELECT 
	       Id
          ,AccountId
          ,SerialNumber
		  ,DeviceKey
		  ,DeviceModelId
          ,FirmwareVersion
          ,[Name]
          ,[Description]
          ,LocationId
          ,PredecessorSerialNumber
          ,PredecessorDate
          ,LastNistSync
          ,IsActive
          ,LoggingInterval
          ,Altitude
          ,HasLostConnectivity
          ,CreatedAt
          ,CreatedBy
          ,ModifiedAt
          ,ModifiedBy
          ,LastLowBatteryAlarm
          ,Timezone
          ,Battery
          ,Wifi
          ,Latest
          ,LastUpdated
          ,CO2LoggingInterval
          ,IsAlarmRepostEnabled
          ,AlarmRepostInterval
	FROM
	      Device
	WHERE 
	      Id=@Id

    SELECT 
	      Id,
		  UserId,
		  DeviceId,
		  CouponId,
		  CouponCode,
		  AppliedDate,
		  ExpiryDate
	FROM
	      DeviceCoupon
	WHERE
	      DeviceId=@Id 
		  AND
		  ExpiryDate > GETDATE() 

END
GO


