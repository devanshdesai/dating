/****** Object:  StoredProcedure [dbo].[AspNetUserActivity_Upsert]    Script Date: 12-02-2021 07:29:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[AspNetUserActivity_Upsert]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AspNetUserActivity_Upsert]
GO

--select * from UserPreference
--exec [AspNetUserActivity_Upsert] 0,1565,'ABC'
CREATE PROCEDURE [dbo].[AspNetUserActivity_Upsert]
	@Id bigint = 0,
	@UserId bigint = 0,
	@Activity varchar(MAX) = null,
	@BearerToken varchar(MAX) = null,
	@IpAddress varchar(50) = null
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					--IF @UserId = 0
					--BEGIN
					--		SET @Id = 0
					--		SET @Code = 400
					--		SET @Message = 'UserId is required'
					--END
					--ELSE IF @Activity is null or @Activity = ''
					--BEGIN
					--		SET @Id = 0
					--		SET @Code = 400
					--		SET @Message = 'Activity is required'
					--END

					--ELSE
					BEGIN
							INSERT INTO AspNetUserActivity (UserId,
															Activity,
															CreatedDate,
															BearerToken,
															IpAddress )
									VALUES (@UserId,
											@Activity,
											GETDATE(),
											@BearerToken,
											@IpAddress)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'AspNetUserActivity Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					--IF @UserId = 0
					--BEGIN
					--		SET @Id = 0
					--		SET @Code = 400
					--		SET @Message = 'UserId is required'
					--END
					--ELSE IF @Activity is null or @Activity = ''
					--BEGIN
					--		SET @Id = 0
					--		SET @Code = 400
					--		SET @Message = 'Activity is required'
					--END

					--ELSE
					BEGIN
							UPDATE AspNetUserActivity
							SET		UserId = @UserId,
									Activity = ISNULL(@Activity,Activity)

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'AspNetUserActivity Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[UserId],
				[Activity],
				[CreatedDate]
		FROM
				[dbo].[AspNetUserActivity]
		WHERE
				Id = @Id

END
GO


