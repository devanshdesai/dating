/****** Object:  StoredProcedure [dbo].[NotificationSettings_All]    Script Date: 12-02-2021 07:49:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[NotificationSettings_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[NotificationSettings_All]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec NotificationSettings_All '',0,0,1508,967,1444
CREATE PROCEDURE [dbo].[NotificationSettings_All] 
	@Search VARCHAR(MAX),
	@Offset INT,
	@Limit INT,
	@UserId BIGINT = 0,
	@AccountId BIGINT = 0,
	@LoggedUserId BIGINT = 0
AS
BEGIN

	SET NOCOUNT ON;
    
	
	DECLARE @TotalRecords bigint,
			@IsAdmin bit;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@LoggedUserId AND IsDeactivated=0 --AND LockoutEnabled=0

	IF @IsAdmin IS NULL OR (@IsAdmin = 0 AND @LoggedUserId != @UserId)
	BEGIN
	      SET @UserId=0
	END

	SET @TotalRecords = (
	                        SELECT 
								  count(*)
	                        FROM
	                               NotificationSettings NS
	                        LEFT JOIN
	                               UserLocationMap ULM
	                        ON     NS.UserLocationId = ULM.Id
	                        LEFT JOIN
	                               [Location] LOC
	                        ON     ULM.LocationId = LOC.Id
	                        WHERE 
	                               ULM.UserId IN (SELECT Id FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 AND AccountId=@AccountId)
	                        	   AND 
	                               ULM.IsDeleted = 0
	                               AND
	                        	   LOC.IsActive = 1
	                        	   AND 
	                        	   NS.IsDeleted = 0
	                        	   AND 
	                        	   (                         
	                        	     ISNULL(@Search,'') = '' OR
	                        		 (
	                        	       LOWER(LOC.Name) LIKE '%'+LOWER(@Search)+'%'               
	                        		 )
	                        	   ) 
	                    )


	IF @Limit = 0
	BEGIN
			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN	
				SET @Limit = 10
			END
	END
	

	SELECT 
	       NS.Id,
		   NS.SmsEnabled,
		   NS.EmailEnabled,
		   NS.PushEnabled,
		   LOC.[Name] as LocationName
	FROM
	       NotificationSettings NS
	LEFT JOIN
	       UserLocationMap ULM
	ON     NS.UserLocationId = ULM.Id
	LEFT JOIN
	       [Location] LOC
	ON     ULM.LocationId = LOC.Id
	WHERE 
	       ULM.UserId IN (SELECT Id FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 AND AccountId=@AccountId)
		   AND 
	       ULM.IsDeleted = 0
	       AND
		   LOC.IsActive = 1
		   AND 
		   NS.IsDeleted = 0
		   AND 
		   (                         
		     ISNULL(@Search,'') = '' OR
			 (
		       LOWER(LOC.Name) LIKE '%'+LOWER(@Search)+'%'               
			 )
		  )
	 order by LOC.[Name]
     ASC
     OFFSET @offset rows fetch next @Limit rows only
	       
	 SELECT @TotalRecords as TotalRecords

END
GO


