/****** Object:  StoredProcedure [dbo].[EscalationData_All]    Script Date: 12-02-2021 08:43:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[EscalationData_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EscalationData_All]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EscalationData_All]
	@Search VARCHAR(MAX) = NULL,
	@Offset INT = 0,
	@Limit INT = 0,
	@AccountId BIGINT = 0,
	@UserId BIGINT = 0,
	@SerialNumber varchar(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @TotalRecords bigint,
			@IsAdmin bit;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 --AND LockoutEnabled=0

	IF @IsAdmin IS NULL OR @IsAdmin = 0
	BEGIN
	     SET @AccountId = -1
	END

	SET @TotalRecords = (
						  SELECT 
						         COUNT(*)
						   FROM    
						         EscalationData
                           WHERE 
                    		   AccountId = ISNULL(NULLIF(@AccountId,0),AccountID)
							   AND SerialNumber = ISNULL(NULLIF(@SerialNumber,''),SerialNumber)
							   AND ISNULL(IsDeleted,0) = 0
						)


	IF @Limit = 0
	BEGIN
			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN	
				SET @Limit = 10
			END
	END

	SELECT
	       ed.*,
		   ds.ChannelName,
		   ds.DataType,
		   (select ui.item as UserID, u.Email
			from dbo.SplitString(ed.userids,',') ui
			inner join AspNetUsers u on u.ID = ui.item
		    for xml raw('User'), ROOT ('Users'),Elements) as Emails
    FROM
	      EscalationData ed
		  left join devicesetting ds on ds.ID = ed.DeviceSettingID
	WHERE 
		ed.AccountId = ISNULL(NULLIF(@AccountId,0),ed.AccountID)
		AND ed.SerialNumber = ISNULL(NULLIF(@SerialNumber,''),ed.SerialNumber)
		AND ISNULL(IsDeleted,0) = 0
	order by ed.ID desc
	OFFSET @offset rows fetch next @Limit rows only

	SELECT @TotalRecords as TotalRecords
END
GO


