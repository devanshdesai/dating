/****** Object:  StoredProcedure [dbo].[NotificationSettings_Logs_All]    Script Date: 12-02-2021 07:48:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[NotificationSettings_Logs_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[NotificationSettings_Logs_All]
GO

CREATE PROCEDURE [dbo].[NotificationSettings_Logs_All]
	@Search varchar(MAX) = '',
	@Offset int = 0,
	@Limit int = 0,
	@UserId bigint = 0,
	@AccountId bigint = 0,
	@ActionType varchar(50) = NULL,
	@FromDate DateTime = NULL,
	@ToDate DateTime = NULL
AS
BEGIN

		SET NOCOUNT ON;

		if(ISNULL(@ToDate,'') <> '')
		BEGIN
			select @ToDate = Convert(date,@ToDate)
			Select @ToDate = dateadd(hh,23,@ToDate)
			Select @ToDate = dateadd(mi,59,@ToDate)
			Select @ToDate = dateadd(ss,59,@ToDate)
		END

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[NotificationSettings_Logs] A
								where (@UserId = 0 or  A.ActionBy = @UserId)
								AND A.ActionDate >= ISNULL(NULLIF(@FromDate,''),A.ActionDate)  AND A.ActionDate <= ISNULL(NULLIF(@ToDate,''),A.ActionDate)
								AND A.ActionType = ISNULL(NULLIF(@ActionType,''), A.ActionType)
								AND A.AccountID = ISNULL(NULLIF(@AccountId,0), A.AccountID)
								and
								(
									isnull(@Search,'') = '' or
									(
										(lower(A.ActionBy) like '%'+lower(@Search)+'%')
									)
								)
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				ID
				NotificationSettingID,
				SmsEnabled,
				EmailEnabled,
				PushEnabled,
				UserLocationId,
				IsDeleted,
				ActionType,
				ActionBy,
				ActionDate,
				IpAddress,
				AccountID,
				ActionByEmail,
				LocationName,
				Email,
				Browser,
				OS
			FROM [dbo].[NotificationSettings_Logs] A with(nolock)
			where (@UserId = 0 or  A.ActionBy = @UserId)
			AND A.ActionDate >= ISNULL(NULLIF(@FromDate,''),A.ActionDate)  AND A.ActionDate <= ISNULL(NULLIF(@ToDate,''),A.ActionDate)
			AND A.ActionType = ISNULL(NULLIF(@ActionType,''), A.ActionType)
			AND A.AccountID = ISNULL(NULLIF(@AccountId,0), A.AccountID)
			and
			(
				isnull(@Search,'') = '' or
				(
					(lower(A.ActionBy) like '%'+lower(@Search)+'%')
				)
			)
			order by A.Id 
			
			desc
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO


