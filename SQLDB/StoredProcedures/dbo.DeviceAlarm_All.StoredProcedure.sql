/****** Object:  StoredProcedure [dbo].[DeviceAlarm_All]    Script Date: 12-02-2021 08:35:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[DeviceAlarm_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeviceAlarm_All]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeviceAlarm_All]
@Search VARCHAR(MAX)='',
@Offset INT,
@Limit INT,
@AccountId BIGINT,
@UserId BIGINT,
@SerialNumber NVARCHAR(50)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TotalRecords bigint,
				@IsAdmin bit;

    SET @TotalRecords = (
	                       SELECT 
						         COUNT(*) 
						   FROM 
						         DeviceAlarm
						   WHERE 
						         SerialNumber = @SerialNumber
								 AND
								 AccountId = @AccountId
	                    )
    

	IF @Limit = 0
	BEGIN
			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
				SET @Limit = 10
			END
	END

	SELECT  
	        Id
           ,Channel
           ,EventType
           ,[Timestamp]
           ,AlarmData
           ,Comments
           ,CreatedAt
           ,CreatedBy
           ,ModifiedAt
           ,ModifiedBy
           ,UiType
	FROM 
	      DeviceAlarm
	WHERE 
	      SerialNumber = @SerialNumber
		  AND
		  AccountId = @AccountId
	order by Id
	DESC
	OFFSET @offset rows fetch next @Limit rows only

	SELECT @TotalRecords as TotalRecords

END
GO


