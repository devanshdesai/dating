/****** Object:  StoredProcedure [dbo].[APIJobTransactions_All]    Script Date: 12-02-2021 08:49:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[APIJobTransactions_All]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[APIJobTransactions_All]
GO

CREATE PROCEDURE [dbo].[APIJobTransactions_All]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select jt.ID,
		jt.APIJobID,
		jt.RequestSize,
		jt.StatusCode,
		jt.ResponseHeaders,
		jt.ResponseBody,
		jt.ResponseSize,
		jt.JobGroupCode,
		jt.CreatedDate,
		jm.Title,
		jm.URL,
		jt.SMSText,
		jt.SMSDateTime,
		jt.EmailText,
		jt.EmailDateTime
	from APIJobTransactions jt
	inner join APIJobMaster jm on jm.ID = jt.APIJobID
	order by jt.JobGroupCode ASC
END

GO


