/****** Object:  StoredProcedure [dbo].[APIJobTransactions_BarCharts]    Script Date: 12-02-2021 08:50:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[APIJobTransactions_BarCharts]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[APIJobTransactions_BarCharts]
GO

CREATE PROCEDURE [dbo].[APIJobTransactions_BarCharts]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select jm.ID,jm.Title as Title, CONVERT(varchar(10),jt.CreatedDate,103) + ' ' + Convert(varchar(2), jt.CreatedDate,114) + ' hr' as Labels,
		Count(CASE WHEN jt.StatusCode = 200 THEN 1 END) as Success,
		Count(CASE WHEN jt.StatusCode <> 200 THEN 1 END) as Failed
	from APIJobTransactions jt
	inner join APIJobMaster jm on jm.ID = jt.APIJobID
	group by jm.ID, jm.Title,CONVERT(varchar(10),jt.CreatedDate,103), Convert(varchar(2), jt.CreatedDate,114)
END
GO


