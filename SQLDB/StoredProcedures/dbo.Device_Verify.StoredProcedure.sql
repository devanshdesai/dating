/****** Object:  StoredProcedure [dbo].[Device_Verify]    Script Date: 12-02-2021 08:32:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from dbo.sysobjects where id= OBJECT_ID(N'[dbo].[Device_Verify]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Device_Verify]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- select * from ChannelSpecification
-- exec Device_Verify 2,1142,'100000006','77711123'
CREATE PROCEDURE [dbo].[Device_Verify] 
@AccountId BIGINT,
@UserId BIGINT,
@SerialNumber nvarchar(50),
@DeviceKey nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @Code INT = 400,
	        @Message VARCHAR(250) = '',
			@IsAdmin bit,
			@Model NVARCHAR(250) = '',
			@DeviceModelId BIGINT = 0;

	SELECT @IsAdmin = IsAdmin FROM AspNetUsers WHERE Id=@UserId AND IsDeactivated=0 AND AccountId=@AccountId --AND LockoutEnabled=0 

	-- check if an admin is accessing
	IF @IsAdmin IS NULL OR @IsAdmin = 0 
	BEGIN
	     SET @Code = 400
		 SET @Message = 'Not allowed to add a device'
	END
	ELSE
	BEGIN
	      IF (SELECT COUNT(*) FROM DEVICE WHERE LOWER(SerialNumber) = LOWER(@SerialNumber)) = 0 -- check if serial number exists in the device
		  BEGIN
               -- check if serial number and device key exists in DeviceModelSerialNumberKeyMap
			   SELECT @Model = Model FROM DeviceModelSerialNumberKeyMap WHERE SerialNumber = @SerialNumber AND DeviceKey = @DeviceKey
			   
			   IF @Model IS NOT NULL AND @Model != ''
			   BEGIN
			         SELECT @DeviceModelId = Id FROM DeviceModel WHERE Model = @Model
					 SET @Code = 200
					 SET @Message = 'Serial Number and Device Key matched'
			   END
			   ELSE
			   BEGIN
			        SET @Code = 400
					SET @Message = 'Serial Number and Device Key does not match'
			   END
		  END 
		  ELSE
		  BEGIN
		  	    SET @Code = 400
				SET @Message = 'Device already exists'
		  END 
	END

	SELECT @Code as Code,
		   @Message as [Message]

	SELECT 
	       Id
          ,Model
          ,[Name]
          ,DeviceType
          ,MaxBattery
          ,MinBattery
          ,LowBattery
          ,isEthernetDevice
          ,isMqtt
          ,IsEthernetEnabled
          ,IsBTHub
		  ,(select top 1 DeviceThumbnailImage from DeviceModelDetails where DeviceModelId = DeviceModel.Id) as DeviceThumbnailImage
	FROM
	       DeviceModel
	WHERE 
	       Id=@DeviceModelId

	SELECT 
	        Id
           ,ChannelName
           ,DataType
           ,[Max]
           ,[Min]
           ,DeviceModelId
	FROM
	      ChannelSpecification
	WHERE 
	      DeviceModelId=@DeviceModelId
	ORDER 
	      BY
	ChannelName ASC


	-- fetch the user preferences for this user
	SELECT
	        UP.Id
		   ,UP.UserId
		   ,UP.DataTypeName
		   ,UP.UoMId
		   ,UP.CreatedAt
		   ,UP.CreatedBy
		   ,UP.ModifiedBy
		   ,UP.ModifiedAt
		   ,UOM.UnitName
		   ,UOM.UnitLabel
		   ,UOM.Resolution
	FROM   
	      UserPreference UP
	LEFT JOIN 
	      UnitOfMeasure UOM
	ON    UP.UoMId = UOM.Id
	WHERE 
	      UP.UserId = @UserId

END
GO


