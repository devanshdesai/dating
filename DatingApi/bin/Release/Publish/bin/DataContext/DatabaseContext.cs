﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Text;
using Dating.Common;
using Dating.Entities.Contract;
using Dating.Entities.V1;

namespace Dating.DAL.DataContext
{
    public class DatabaseContext : DbContext
    {
        //public class OptionsBuild
        //{
        //    public DbContextOptionsBuilder<DatabaseContext> opsBuilder { get; set; } 
        //    public DbContextOptions<DatabaseContext> dbOptions { get; set; }
        //    private AppConfiguration settings { get; set; }

        //    public OptionsBuild()
        //    {
        //        settings = new AppConfiguration();
        //        opsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
        //        opsBuilder.UseSqlServer(settings.sqlConnectionString);
        //        dbOptions = opsBuilder.Options;
        //    }
        //}

        //public static OptionsBuild ops = new OptionsBuild();   

        public DatabaseContext() : base("ShareRegistryDBContext")
        {
            Database.SetInitializer<DatabaseContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Location>().Ignore(t => t.TotalDeviceCount);
            modelBuilder.Entity<Location>().Ignore(t => t.TotalUserCount);

            modelBuilder.Entity<AspNetUsers>().Ignore(t => t.TotalLocationCount);
            modelBuilder.Entity<AspNetUsers>().Ignore(t => t.IsInEscalation);
            modelBuilder.Entity<AspNetUsers>().Ignore(t => t.LastLogin);
            modelBuilder.Entity<AspNetUsers>().Ignore(t => t.IsSMSVerified);
            modelBuilder.Entity<AspNetUsers>().Ignore(t => t.IsEmailVerified);

            modelBuilder.Entity<NotificationSettings>().Ignore(t => t.LocationName);

            modelBuilder.Entity<UserLocationMap>().Ignore(t => t.UserEmail);
            modelBuilder.Entity<UserLocationMap>().Ignore(t => t.UserName);
            modelBuilder.Entity<UserLocationMap>().Ignore(t => t.IsAdmin);

            modelBuilder.Entity<UserPreference>().Ignore(t => t.UnitName);
            modelBuilder.Entity<UserPreference>().Ignore(t => t.UnitLabel);
            modelBuilder.Entity<UserPreference>().Ignore(t => t.Resolution);

            modelBuilder.Entity<Device>().Ignore(t => t.DeviceModelJson);
            modelBuilder.Entity<Device>().Ignore(t => t.DeviceSettingsJson);
            modelBuilder.Entity<Device>().Ignore(t => t.LocationName);
            modelBuilder.Entity<Device>().Ignore(t => t.IsPined);
            modelBuilder.Entity<Device>().Ignore(t => t.DeviceThumbnailImage);
            modelBuilder.Entity<DeviceModel>().Ignore(t => t.DeviceThumbnailImage);

            modelBuilder.Entity<DeviceSetting>().Ignore(t => t.ChannelSpecificationMin);
            modelBuilder.Entity<DeviceSetting>().Ignore(t => t.ChannelSpecificationMax);

            modelBuilder.Entity<Location>().Ignore(t => t.LocationPhoto);

            modelBuilder.Entity<AspNetUsers_Logs>().Ignore(t => t.ActionByEmail);

            //modelBuilder.Entity<AspNetUserPasswordHistories_Logs>().Ignore(t => t.ActionByEmail);

            //modelBuilder.Entity<NotificationSettings_Logs>().Ignore(t => t.ActionByEmail);
            //modelBuilder.Entity<NotificationSettings_Logs>().Ignore(t => t.Email);
            //modelBuilder.Entity<NotificationSettings_Logs>().Ignore(t => t.LocationName);

            //modelBuilder.Entity<UserLocationMap_Logs>().Ignore(t => t.ActionByEmail);
            //modelBuilder.Entity<UserLocationMap_Logs>().Ignore(t => t.Email);
            //modelBuilder.Entity<UserLocationMap_Logs>().Ignore(t => t.LocationName);

            modelBuilder.Entity<EscalationData>().Ignore(t => t.Emails);

            modelBuilder.Entity<DocListing>().Ignore(t => t.FirstName);
            modelBuilder.Entity<DocListing>().Ignore(t => t.LastName);
            modelBuilder.Entity<DocListing>().Ignore(t => t.Email);

            modelBuilder.Entity<DeviceSettingsActivity>().Ignore(t => t.CreatedByEmail);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<AspNetUsers> AspNetUsers { get; set; }
        public DbSet<LoginHistories> LoginHistories { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<AspNetUserPasswordHistories> AspNetUserPasswordHistories { get; set; }
        public DbSet<UserPreference> UserPreference { get; set; }
        public DbSet<UserLocationMap> UserLocationMap { get; set; }
        public DbSet<UnitOfMeasure> UnitOfMeasures { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<NotificationSettings> NotificationSettings { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<DeviceSetting> DeviceSettings { get; set; }
        public DbSet<DeviceModelSerialNumberKeyMap> DeviceModelSerialNumberKeyMaps { get; set; }
        public DbSet<ChannelSpecification> ChannelSpecifications { get; set; }
        public DbSet<DeviceModel> DeviceModels { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<EscalationData> EscalationData { get; set; }
        public DbSet<EscalationService> EscalationService { get; set; }
        public DbSet<APIJobMaster> APIJobMasters { get; set; }
        public DbSet<APIJobTransactions> APIJobTransactions { get; set; }
        public DbSet<DeviceAlarm> DeviceAlarms { get; set; }
        public DbSet<AspNetUserReset> AspNetUserReset { get; set; }

        //public DbSet<AspNetUsers_Logs> AspNetUsers_Logs { get; set; }
        //public DbSet<NotificationSettings_Logs> NotificationSettings_Logs { get; set; }
        //public DbSet<AspNetUserPasswordHistories_Logs> AspNetUserPasswordHistories_Logs { get; set; }
        //public DbSet<UserLocationMap_Logs> UserLocationMap_Logs { get; set; }
        //public DbSet<DeviceSetting_Log> DeviceSetting_Log { get; set; }
        public DbSet<LocationImages> LocationImages { get; set; }
        public DbSet<AspNetUsersPhoneNumbers> AspNetUsersPhoneNumbers { get; set; }
        public DbSet<AspNetUsersEmails> AspNetUsersEmails { get; set; }
        public DbSet<DocListing> DocListing { get; set; }
        public DbSet<DeviceUsers> DeviceUsers { get; set; }
        public DbSet<DeviceSettingsActivity> DeviceSettingsActivity { get; set; }
    }
}
