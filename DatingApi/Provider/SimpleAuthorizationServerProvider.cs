﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
//using Dating.Data.Contract;
//using Dating.Entities.Contract;
//using Dating.Entities.V1;
//using Dating.Services.Contract;
//using Dating.Services.V1;
//using DatingApi.Controllers.V1;

namespace DatingApi.Provider
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
       
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string IpAddress = context.Parameters.Where(f => f.Key == "IpAddress").Select(f => f.Value).SingleOrDefault()[0];
            context.OwinContext.Set<string>("IpAddress", IpAddress);
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            
            //AbstractAspNetUsers abstractAspNetUsers = new AspNetUsers();
            //abstractAspNetUsers.Email = context.UserName;
            //abstractAspNetUsers.EnteredPassword = context.Password;
            //abstractAspNetUsers.IpAddress = context.Request.RemoteIpAddress; //context.OwinContext.Get<string>("IpAddress");

            //AspNetUsersServices_Token aspNetUsersServices_Token = new AspNetUsersServices_Token();

            //var result = await aspNetUsersServices_Token.AspNetUsers_VerifyUsernameEmail(abstractAspNetUsers);

            //if (result.Code != 200)
            //{
            //    context.SetError("invalid_grant", result.Message);
            //    return;
            //}

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim("UserID", Convert.ToString(0))); //result.Item.Id
            identity.AddClaim(new Claim("AccountID", Convert.ToString(0))); //result.Item.AccountId
            //identity.AddClaim(new Claim("role", "user"));

            context.Validated(identity);
        }

        private void SetCORSPolicy(IOwinContext context)
        {
            //string allowedUrls = ConfigurationManager.AppSettings["allowedOrigins"];

            //if (!String.IsNullOrWhiteSpace(allowedUrls))
            //{
            //    var list = allowedUrls.Split(',');
            //    if (list.Length > 0)
            //    {

            //        string origin = context.Request.Headers.Get("Origin");
            //        var found = list.Where(item => item == origin).Any();
            //        if (found)
            //        {
            //            context.Response.Headers.Add("Access-Control-Allow-Origin",
            //                                         new string[] { origin });
            //        }
            //    }

            //}
            context.Response.Headers.Add("Access-Control-Allow-Origin",
                                         new string[] { "*" });
            context.Response.Headers.Add("Access-Control-Allow-Headers",
                                   new string[] { "Authorization", "Content-Type" });
            context.Response.Headers.Add("Access-Control-Allow-Methods",
                                   new string[] { "OPTIONS", "POST" });

        }
    }
}