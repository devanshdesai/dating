﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using TraceableLive.APICommon;
using TraceableLive.Common;
using TraceableLive.Common.Paging;
using TraceableLive.Entities.Contract;
using TraceableLive.Entities.V1;
using TraceableLive.Services.Contract;

namespace TraceableLiveApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LocationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLocationServices abstractLocationServices;
        private readonly AbstractAspNetUserActivityServices abstractAspNetUserActivityServices;
        private readonly AbstractLocationImagesServices abstractLocationImagesServices;
        private readonly AbstractAspNetUsersServices abstractAspNetUsersServices;
        private readonly AbstractUserLocationMap_LogsServices abstractUserLocationMap_LogsServices;
        private readonly AbstractLocation_LogsServices abstractLocation_LogsServices;
        private string Container = Configurations.tlui2Container;
        #endregion

        #region Cnstr
        public LocationV1Controller(AbstractLocationServices _abstractLocationServices,
            AbstractAspNetUserActivityServices abstractAspNetUserActivityServices,
            AbstractLocationImagesServices abstractLocationImagesServices,
            AbstractAspNetUsersServices abstractAspNetUsersServices,
            AbstractUserLocationMap_LogsServices abstractUserLocationMap_LogsServices,
            AbstractLocation_LogsServices abstractLocation_LogsServices)
        {
            this.abstractLocationServices = _abstractLocationServices;
            this.abstractAspNetUserActivityServices = abstractAspNetUserActivityServices;
            this.abstractLocationImagesServices = abstractLocationImagesServices;
            this.abstractAspNetUsersServices = abstractAspNetUsersServices;
            this.abstractUserLocationMap_LogsServices = abstractUserLocationMap_LogsServices;
            this.abstractLocation_LogsServices = abstractLocation_LogsServices;
        }
        #endregion

        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [InheritedRoute("Location_All")]
        public async Task<IHttpActionResult> Location_All(PageParam pageParam, string search = "")
        {
            PagedList<AbstractLocation> result = new PagedList<AbstractLocation>();

            try
            {
                var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;
                Int64 UserID = Convert.ToInt64(claimsIdentity.FindFirst("UserID").Value);
                Int64 AccountID = Convert.ToInt64(claimsIdentity.FindFirst("AccountID").Value);

                result = abstractLocationServices.Location_All(pageParam, search, AccountID, UserID);

                var re = Request;
                var headers = re.Headers;

                string token = "";
                if (headers.Contains("Authorization"))
                {
                    token = headers.GetValues("Authorization").First();
                }

                string clientAddress = GetIp();

                AbstractAspNetUserActivity abstractAspNetUserActivity = new AspNetUserActivity();
                abstractAspNetUserActivity.UserID = UserID;
                abstractAspNetUserActivity.Activity = "Viewed the locations list";
                abstractAspNetUserActivity.BearerToken = token;
                abstractAspNetUserActivity.IpAddress = clientAddress;
                abstractAspNetUserActivityServices.AspNetUserActivity_Upsert(abstractAspNetUserActivity);
            }
            catch (Exception ex)
            {

            }

            return this.Content((HttpStatusCode)200, result);
        }

        [System.Web.Http.Authorize]
        [System.Web.Http.HttpGet]
        [InheritedRoute("Location_ById")]
        public async Task<IHttpActionResult> Location_ById(Int64 ID)
        {
            SuccessResult<AbstractLocation> result = new SuccessResult<AbstractLocation>();

            try
            {
                var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;
                Int64 UserID = Convert.ToInt64(claimsIdentity.FindFirst("UserID").Value);
                Int64 AccountID = Convert.ToInt64(claimsIdentity.FindFirst("AccountID").Value);

                result = abstractLocationServices.Location_ById(ID, AccountID, UserID);

                if (result.Code == 200 && result.Item != null)
                {
                    var re = Request;
                    var headers = re.Headers;

                    string token = "";
                    if (headers.Contains("Authorization"))
                    {
                        token = headers.GetValues("Authorization").First();
                    }

                    string clientAddress = GetIp();

                    AbstractAspNetUserActivity abstractAspNetUserActivity = new AspNetUserActivity();
                    abstractAspNetUserActivity.UserID = UserID;
                    abstractAspNetUserActivity.Activity = "Viewed the details of the location @" + result.Item.Name;
                    abstractAspNetUserActivity.BearerToken = token;
                    abstractAspNetUserActivity.IpAddress = clientAddress;

                    abstractAspNetUserActivityServices.AspNetUserActivity_Upsert(abstractAspNetUserActivity);
                }
            }
            catch (Exception ex)
            {
                result.Code = 400;
                result.Message = ex.Message;
            }

            return this.Content((HttpStatusCode)result.Code, result);
        }

        [System.Web.Http.Authorize]
        [System.Web.Http.HttpGet]
        [InheritedRoute("Location_InActive")]
        public async Task<IHttpActionResult> Location_InActive(Int64 ID)
        {
            SuccessResult<AbstractLocation> result = new SuccessResult<AbstractLocation>();

            try
            {
                var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;
                Int64 UserID = Convert.ToInt64(claimsIdentity.FindFirst("UserID").Value);
                Int64 AccountID = Convert.ToInt64(claimsIdentity.FindFirst("AccountID").Value);

                result = abstractLocationServices.Location_InActive(ID, AccountID, UserID);

                if (result.Code == 200 && result.Item != null)
                {
                    var re = Request;
                    var headers = re.Headers;

                    string token = "";
                    if (headers.Contains("Authorization"))
                    {
                        token = headers.GetValues("Authorization").First();
                    }

                    string clientAddress = GetIp();

                    AbstractAspNetUserActivity abstractAspNetUserActivity = new AspNetUserActivity();
                    abstractAspNetUserActivity.UserID = UserID;
                    abstractAspNetUserActivity.Activity = "Removed the location @" + result.Item.Name;
                    abstractAspNetUserActivity.BearerToken = token;
                    abstractAspNetUserActivity.IpAddress = clientAddress;
                    abstractAspNetUserActivityServices.AspNetUserActivity_Upsert(abstractAspNetUserActivity);
                }
            }
            catch (Exception ex)
            {
                result.Code = 400;
                result.Message = ex.Message;
            }

            return this.Content((HttpStatusCode)result.Code, result);
        }

        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [InheritedRoute("Location_Upsert")]
        public async Task<IHttpActionResult> Location_Upsert(Location location)
        {
            SuccessResult<AbstractLocation> result = new SuccessResult<AbstractLocation>();

            try
            {
                var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;
                Int64 UserID = Convert.ToInt64(claimsIdentity.FindFirst("UserID").Value);
                Int64 AccountID = Convert.ToInt64(claimsIdentity.FindFirst("AccountID").Value);

                bool isNew = true;
                if (location.Id > 0)
                {
                    isNew = false;
                }

                result = abstractLocationServices.Location_Upsert(location, AccountID, UserID);

                if (result.Code == 200 && result.Item != null)
                {
                    var re = Request;
                    var headers = re.Headers;

                    string token = "";
                    if (headers.Contains("Authorization"))
                    {
                        token = headers.GetValues("Authorization").First();
                    }

                    string clientAddress = GetIp();

                    AbstractAspNetUserActivity abstractAspNetUserActivity = new AspNetUserActivity();
                    abstractAspNetUserActivity.UserID = UserID;
                    abstractAspNetUserActivity.Activity = (isNew == true) ? "Added a new location @" + result.Item.Name : "Edited the details for the location @" + result.Item.Name;
                    abstractAspNetUserActivity.BearerToken = token;
                    abstractAspNetUserActivity.IpAddress = clientAddress;
                    abstractAspNetUserActivityServices.AspNetUserActivity_Upsert(abstractAspNetUserActivity);
                }
            }
            catch (Exception ex)
            {
                result.Code = 400;
                result.Message = ex.Message;
            }

            return this.Content((HttpStatusCode)result.Code, result);
        }

        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [InheritedRoute("Location_UploadImage")]
        public async Task<IHttpActionResult> Location_UploadImage(int LocationId)
        {
            SuccessResult<AbstractLocationImages> result = new SuccessResult<AbstractLocationImages>();

            try
            {
                var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;
                Int64 UserID = Convert.ToInt64(claimsIdentity.FindFirst("UserID").Value);
                Int64 AccountID = Convert.ToInt64(claimsIdentity.FindFirst("AccountID").Value);

                var userResult = abstractAspNetUsersServices.AspNetUsers_ById(UserID, AccountID, UserID);

                if (userResult.Item != null && userResult.Item.IsAdmin)
                {
                    if (!Request.Content.IsMimeMultipartContent("form-data"))
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }

                    var httpRequest = HttpContext.Current.Request;
                    var accountName = Configurations.tlui2AccountName;
                    var accountKey = Configurations.tlui2AccountKey;
                    var storageAccount = new CloudStorageAccount(new StorageCredentials(accountName, accountKey), true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    CloudBlobContainer imagesContainer = blobClient.GetContainerReference(Container);
                    var provider = new AzureStorageMultipartFormDataStreamProvider(imagesContainer);

                    try
                    {
                        await Request.Content.ReadAsMultipartAsync(provider);
                    }
                    catch (Exception ex)
                    {
                        return BadRequest($"An error has occured. Details: {ex.Message}");
                    }

                    var filename = provider.FileData.FirstOrDefault()?.LocalFileName;
                    if (string.IsNullOrEmpty(filename))
                    {
                        return BadRequest("An error has occured while uploading your file. Please try again.");
                    }
                    else
                    {
                        LocationImages locationImages = new LocationImages();
                        locationImages.LocationId = LocationId;
                        locationImages.LocationPhoto = filename;
                        locationImages.CreatedBy = UserID;
                        locationImages.CreatedDate = DateTime.UtcNow;
                        result = abstractLocationImagesServices.LocationImages_Upsert(locationImages);
                    }
                }
                else
                {
                    result.Code = 400;
                    result.Message = "Not authorized to upload image";
                }
            }
            catch (Exception ex)
            {
                result.Code = 400;
                result.Message = "";
            }

            return this.Content((HttpStatusCode)result.Code, result);
        }

        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserLocationMap_Logs_All")]
        public async Task<IHttpActionResult> UserLocationMap_Logs_All(PageParam pageParam, long userID = 0, string actionType = "", DateTime? fromDate = null, DateTime? toDate = null)
        {
            PagedList<AbstractUserLocationMap_Logs> result = new PagedList<AbstractUserLocationMap_Logs>();

            try
            {
                var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;
                Int64 AccountID = Convert.ToInt64(claimsIdentity.FindFirst("AccountID").Value);

                result = abstractUserLocationMap_LogsServices.UserLocationMap_Logs_All(pageParam, "", userID, AccountID, actionType, fromDate, toDate);
            }
            catch (Exception ex)
            {
            }

            return this.Content((HttpStatusCode)200, result);
        }

        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [InheritedRoute("Location_Logs_All")]
        public async Task<IHttpActionResult> Location_Logs_All(PageParam pageParam, long userID = 0, string actionType = "", DateTime? fromDate = null, DateTime? toDate = null)
        {
            PagedList<AbstractLocation_Logs> result = new PagedList<AbstractLocation_Logs>();

            try
            {
                var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;
                Int64 AccountID = Convert.ToInt64(claimsIdentity.FindFirst("AccountID").Value);

                result = abstractLocation_LogsServices.Location_Logs_All(pageParam, "", userID, AccountID, actionType, fromDate, toDate);
            }
            catch (Exception ex)
            {
            }

            return this.Content((HttpStatusCode)200, result);
        }

        private string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }

        //private string GetClientIpString(HttpRequestMessage request)
        //{
        //    //Web-hosting
        //    if (request.Properties.ContainsKey("MS_HttpContext"))
        //    {
        //        dynamic ctx = request.Properties["MS_HttpContext"];
        //        if (ctx != null)
        //        {
        //            return ctx.Request.UserHostAddress;
        //        }
        //    }
        //    //Self-hosting
        //    if (request.Properties.ContainsKey("System.ServiceModel.Channels.RemoteEndpointMessageProperty"))
        //    {
        //        dynamic remoteEndpoint = request.Properties["System.ServiceModel.Channels.RemoteEndpointMessageProperty"];
        //        if (remoteEndpoint != null)
        //        {
        //            return remoteEndpoint.Address;
        //        }
        //    }
        //    //Owin-hosting
        //    if (request.Properties.ContainsKey("MS_OwinContext"))
        //    {
        //        dynamic ctx = request.Properties["MS_OwinContext"];
        //        if (ctx != null)
        //        {
        //            return ctx.Request.RemoteIpAddress;
        //        }
        //    }
        //    if (System.Web.HttpContext.Current != null)
        //    {
        //        return System.Web.HttpContext.Current.Request.UserHostAddress;
        //    }
        //    // Always return all zeroes for any failure
        //    return "0.0.0.0";
        //}

        //private IPAddress GetClientIpAddress(HttpRequestMessage request)
        //{
        //    var ipString = GetClientIpString(request);
        //    IPAddress ipAddress = new IPAddress(0);
        //    if (IPAddress.TryParse(ipString, out ipAddress))
        //    {
        //        return ipAddress;
        //    }

        //    return ipAddress;
        //}

        private class AzureStorageMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
        {
            private readonly CloudBlobContainer _blobContainer;
            private readonly string[] _supportedMimeTypes = { "image/png", "image/jpeg", "image/jpg" };

            public AzureStorageMultipartFormDataStreamProvider(CloudBlobContainer blobContainer) : base("azure")
            {
                _blobContainer = blobContainer;
            }

            public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
            {
                if (parent == null) throw new ArgumentNullException(nameof(parent));
                if (headers == null) throw new ArgumentNullException(nameof(headers));

                if (headers.ContentType != null)
                {

                    if (!_supportedMimeTypes.Contains(headers.ContentType.ToString().ToLower()))
                    {
                        throw new NotSupportedException("Only jpeg and png are supported");
                    }

                    // Generate a new filename for every new blob
                    var fileName = Guid.NewGuid().ToString();

                    CloudBlockBlob blob = _blobContainer.GetBlockBlobReference(fileName);

                    if (headers.ContentType != null)
                    {
                        // Set appropriate content type for your uploaded file
                        blob.Properties.ContentType = headers.ContentType.MediaType;
                    }

                    this.FileData.Add(new MultipartFileData(headers, blob.Name));

                    return blob.OpenWrite();
                }
                else
                {
                    return null;
                }
            }
        }

    }
}