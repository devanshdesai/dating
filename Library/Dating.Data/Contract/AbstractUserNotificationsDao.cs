﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Entities.Contract;

namespace Dating.Data.Contract
{
    public abstract class AbstractUserNotificationsDao
    {
        public abstract SuccessResult<AbstractUserNotifications> UserNotifications_Upsert(AbstractUserNotifications abstractUserNotifications);
        public abstract SuccessResult<AbstractUserNotifications> UserNotifications_MarkReadById(int Id);
        public abstract SuccessResult<AbstractUserNotifications> UserNotifications_MarkReadByUserId(int UserId);
        public abstract PagedList<AbstractUserNotifications> UserNotifications_All(PageParam pageParam, string search);



    }
}
