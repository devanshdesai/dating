﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Entities.Contract;

namespace Dating.Data.Contract
{
    public abstract class AbstractUserInterestsDao
    {
        public abstract PagedList<AbstractUserInterests> UserInterests_ByUserId(PageParam pageParam, string search,int UserId);
    }
}
