﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Entities.Contract;

namespace Dating.Data.Contract
{
    public abstract class AbstractMasterLookingForDao
    {
        public abstract PagedList<AbstractMasterLookingFor> MasterLookingFor_All(PageParam pageParam, string search);
       

    }
}
