﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Rushkar">
//     Copyright Rushkar Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dating.Data
{
    using Autofac;
    using Dating.Data.Contract;
    using Dating.Data.V1;

    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<V1.MasterSmokeOptionsDao>().As<AbstractMasterSmokeOptionsDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterBodyTypeDao>().As<AbstractMasterBodyTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterEthnicityDao>().As<AbstractMasterEthnicityDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterOrientationDao>().As<AbstractMasterOrientationDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterFoodPreferenceDao>().As<AbstractMasterFoodPreferenceDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterChildrenOptionsDao>().As<AbstractMasterChildrenOptionsDao>().InstancePerDependency();
            builder.RegisterType<V1.UserGalleryDao>().As<AbstractUserGalleryDao>().InstancePerDependency();
            builder.RegisterType<V1.UserLanguagesDao>().As<AbstractUserLanguagesDao>().InstancePerDependency();
            builder.RegisterType<V1.UserInterestsDao>().As<AbstractUserInterestsDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterTripTypeDao>().As<AbstractMasterTripTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterWhenDao>().As<AbstractMasterWhenDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterWhosPayingDao>().As<AbstractMasterWhosPayingDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterTravelViaDao>().As<AbstractMasterTravelViaDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterMeetingBeforeTripDao>().As<AbstractMasterMeetingBeforeTripDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterAccomodationDao>().As<AbstractMasterAccomodationDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterGenderDao>().As<AbstractMasterGenderDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterInterestsDao>().As<AbstractMasterInterestsDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterCountryDao>().As<AbstractMasterCountryDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterStateDao>().As<AbstractMasterStateDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterCityDao>().As<AbstractMasterCityDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterIdentityLoginsDao>().As<AbstractMasterIdentityLoginsDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterRelationshipStatusDao>().As<AbstractMasterRelationshipStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterLanguagesDao>().As<AbstractMasterLanguagesDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterLookingForDao>().As<AbstractMasterLookingForDao>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationsDao>().As<AbstractUserNotificationsDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterDrinkOptionsDao>().As<AbstractMasterDrinkOptionsDao>().InstancePerDependency();
            base.Load(builder);
        }
    }
}
