﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Entities.V1;
using Dapper;

namespace Dating.Data.V1
{
    public class UserLanguagesDao : AbstractUserLanguagesDao
    {

      

        public override PagedList<AbstractUserLanguages> UserLanguages_ByUserId(PageParam pageParam, string search,int UserId)
        {
            PagedList<AbstractUserLanguages> UserLanguages = new PagedList<AbstractUserLanguages>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserLanguages_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserLanguages.Values.AddRange(task.Read<UserLanguages>());
                UserLanguages.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserLanguages;
        }

       
    }
}
