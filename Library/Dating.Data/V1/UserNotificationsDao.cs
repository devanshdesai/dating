﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Entities.V1;
using Dapper;

namespace Dating.Data.V1
{
    public class UserNotificationsDao : AbstractUserNotificationsDao
    {
        public override SuccessResult<AbstractUserNotifications> UserNotifications_MarkReadById(int Id)
        {
            SuccessResult<AbstractUserNotifications> UserNotifications = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_MarkReadById, param, commandType: CommandType.StoredProcedure);
                UserNotifications = task.Read<SuccessResult<AbstractUserNotifications>>().SingleOrDefault();
                UserNotifications.Item = task.Read<UserNotifications>().SingleOrDefault();
            }

            return UserNotifications;
        }
        public override SuccessResult<AbstractUserNotifications> UserNotifications_MarkReadByUserId(int UserId)
        {
            SuccessResult<AbstractUserNotifications> UserNotifications = null;
            var param = new DynamicParameters();

            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_MarkReadByUserId, param, commandType: CommandType.StoredProcedure);
                UserNotifications = task.Read<SuccessResult<AbstractUserNotifications>>().SingleOrDefault();
                UserNotifications.Item = task.Read<UserNotifications>().SingleOrDefault();
            }

            return UserNotifications;
        }
        public override SuccessResult<AbstractUserNotifications> UserNotifications_Upsert(AbstractUserNotifications abstractUserNotifications)
        {
            SuccessResult<AbstractUserNotifications> UserNotifications = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserNotifications.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserNotifications.UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Description", abstractUserNotifications.Description, dbType: DbType.String, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_Upsert, param, commandType: CommandType.StoredProcedure);
                UserNotifications = task.Read<SuccessResult<AbstractUserNotifications>>().SingleOrDefault();
                UserNotifications.Item = task.Read<UserNotifications>().SingleOrDefault();
            }

            return UserNotifications;
        }


        public override PagedList<AbstractUserNotifications> UserNotifications_All(PageParam pageParam, string search)
        {
            PagedList<AbstractUserNotifications> UserNotifications = new PagedList<AbstractUserNotifications>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_All, param, commandType: CommandType.StoredProcedure);
                UserNotifications.Values.AddRange(task.Read<UserNotifications>());
                UserNotifications.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserNotifications;
        }
    }
}
