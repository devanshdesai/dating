﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Entities.V1;
using Dapper;

namespace Dating.Data.V1
{
    public class MasterCountryDao : AbstractMasterCountryDao
    {
       

        public override PagedList<AbstractMasterCountry> MasterCountry_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterCountry> MasterCountry = new PagedList<AbstractMasterCountry>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCountry_All, param, commandType: CommandType.StoredProcedure);
                MasterCountry.Values.AddRange(task.Read<MasterCountry>());
                MasterCountry.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterCountry;
        }

     
    }
}
