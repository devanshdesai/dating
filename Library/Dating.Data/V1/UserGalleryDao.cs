﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Entities.V1;
using Dapper;

namespace Dating.Data.V1
{
    public class UserGalleryDao : AbstractUserGalleryDao
    {

        public override SuccessResult<AbstractUserGallery> UserGallery_Upsert(AbstractUserGallery abstractUserGallery)
        {
            SuccessResult<AbstractUserGallery> UserGallery = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserGallery.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserGallery.UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ImageUrl", abstractUserGallery.ImageUrl, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserGallery_Upsert, param, commandType: CommandType.StoredProcedure);
                UserGallery = task.Read<SuccessResult<AbstractUserGallery>>().SingleOrDefault();
                UserGallery.Item = task.Read<UserGallery>().SingleOrDefault();
            }

            return UserGallery;
        }

        public override SuccessResult<AbstractUserGallery> UserGallery_Delete(int Id,int DeletedBy)
        {
            SuccessResult<AbstractUserGallery> UserGallery = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserGallery_Delete, param, commandType: CommandType.StoredProcedure);
                UserGallery = task.Read<SuccessResult<AbstractUserGallery>>().SingleOrDefault();
                UserGallery.Item = task.Read<UserGallery>().SingleOrDefault();
            }

            return UserGallery;
        }

        public override PagedList<AbstractUserGallery> UserGallery_All(PageParam pageParam, string search)
        {
            PagedList<AbstractUserGallery> UserGallery = new PagedList<AbstractUserGallery>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserGallery_All, param, commandType: CommandType.StoredProcedure);
                UserGallery.Values.AddRange(task.Read<UserGallery>());
                UserGallery.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserGallery;
        }

        public override PagedList<AbstractUserGallery> UserGallery_ByUserId(PageParam pageParam, string search,int UserId)
        {
            PagedList<AbstractUserGallery> UserGallery = new PagedList<AbstractUserGallery>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserGallery_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserGallery.Values.AddRange(task.Read<UserGallery>());
                UserGallery.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserGallery;
        }


    }
}
