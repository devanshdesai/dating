﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Entities.V1;
using Dapper;

namespace Dating.Data.V1
{
    public class MasterDrinkOptionsDao : AbstractMasterDrinkOptionsDao
    {
       

        public override PagedList<AbstractMasterDrinkOptions> MasterDrinkOptions_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterDrinkOptions> MasterDrinkOptions = new PagedList<AbstractMasterDrinkOptions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterDrinkOptions_All, param, commandType: CommandType.StoredProcedure);
                MasterDrinkOptions.Values.AddRange(task.Read<MasterDrinkOptions>());
                MasterDrinkOptions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterDrinkOptions;
        }

     
    }
}
