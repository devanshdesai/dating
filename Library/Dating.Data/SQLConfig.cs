﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dating.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        #region MasterSmokeOptions
        public const string MasterSmokeOptions_All = "MasterSmokeOptions_All";
        #endregion
        #region MasterBodyType
        public const string MasterBodyType_All = "MasterBodyType_All";
        #endregion
        #region MasterEthnicity
        public const string MasterEthnicity_All = "MasterEthnicity_All";
        #endregion
        #region MasterOrientation
        public const string MasterOrientation_All = "MasterOrientation_All";
        #endregion
        #region MasterFoodPreference
        public const string MasterFoodPreference_All = "MasterFoodPreference_All";
        #endregion
        #region MasterChildrenOptions
        public const string MasterChildrenOptions_All = "MasterChildrenOptions_All";
        #endregion
        #region UserGallery
        public const string UserGallery_All = "UserGallery_All";
        public const string UserGallery_ByUserId = "UserGallery_ByUserId";
        public const string UserGallery_Delete = "UserGallery_Delete";
        public const string UserGallery_Upsert = "UserGallery_Upsert";
        #endregion
        #region UserLanguages
        public const string UserLanguages_ByUserId = "UserLanguages_ByUserId";
        #endregion
        #region UserInterests
        public const string UserInterests_ByUserId = "UserInterests_ByUserId";
        #endregion
        #region MasterTripType
        public const string MasterTripType_All = "MasterTripType_All";
        #endregion
        #region MasterAccomodation
        public const string MasterAccomodation_All = "MasterAccomodation_All";
        #endregion
        #region MasterWhen
        public const string MasterWhen_All = "MasterWhen_All";
        #endregion
        #region MasterWhosPaying
        public const string MasterWhosPaying_All = "MasterWhosPaying_All";
        #endregion
        #region MasterTravelVia
        public const string MasterTravelVia_All = "MasterTravelVia_All";
        #endregion
        #region MasterMeetingBeforeTrip
        public const string MasterMeetingBeforeTrip_All = "MasterMeetingBeforeTrip_All";
        #endregion
        #region MasterGender
        public const string MasterGender_All = "MasterGender_All";
        #endregion
        #region MasterInterests
        public const string MasterInterests_All = "MasterInterests_All";
        #endregion
        #region MasterCountry
        public const string MasterCountry_All = "MasterCountry_All";
        #endregion
        #region MasterState
        public const string MasterState_All = "MasterState_All";
        #endregion
        #region MasterCity
        public const string MasterCity_All = "MasterCity_All";
        #endregion
        #region MasterIdentityLogins
        public const string MasterIdentityLogins_All = "MasterIdentityLogins_All";
        #endregion
        #region MasterRelationshipStatus
        public const string MasterRelationshipStatus_All = "MasterRelationshipStatus_All";
        #endregion
        #region MasterLanguages
        public const string MasterLanguages_All = "MasterLanguages_All";
        #endregion
        #region MasterLookingFor
        public const string MasterLookingFor_All = "MasterLookingFor_All";
        #endregion
        #region MasterDrinkOptions
        public const string MasterDrinkOptions_All = "MasterDrinkOptions_All";
        #endregion
        #region UserNotifications
        public const string UserNotifications_All = "UserNotifications_All";
        public const string UserNotifications_Upsert = "UserNotifications_Upsert";
        public const string UserNotifications_MarkReadById = "UserNotifications_MarkReadById";
        public const string UserNotifications_MarkReadByUserId = "UserNotifications_MarkReadByUserId";
        #endregion
    }
}
