﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dating.Entities.Contract
{
    public abstract class AbstractMasterBodyType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
