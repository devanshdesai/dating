﻿using Dating.Common;
using Dating.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dating.Entities.Contract
{
    public abstract class AbstractUserInterests
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int MasterInterestsId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
    }
}
