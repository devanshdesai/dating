﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterCountryServices : AbstractMasterCountryServices
    {
        private AbstractMasterCountryDao abstractMasterCountryDao;

        public MasterCountryServices(AbstractMasterCountryDao abstractMasterCountryDao)
        {
            this.abstractMasterCountryDao = abstractMasterCountryDao;
        }

     
        
        public override PagedList<AbstractMasterCountry> MasterCountry_All(PageParam pageParam, string search)
        {
            return this.abstractMasterCountryDao.MasterCountry_All(pageParam, search);
        }


        
    }
}
    