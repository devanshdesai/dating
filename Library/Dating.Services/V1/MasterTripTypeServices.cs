﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterTripTypeServices : AbstractMasterTripTypeServices
    {
        private AbstractMasterTripTypeDao abstractMasterTripTypeDao;

        public MasterTripTypeServices(AbstractMasterTripTypeDao abstractMasterTripTypeDao)
        {
            this.abstractMasterTripTypeDao = abstractMasterTripTypeDao;
        }
        public override PagedList<AbstractMasterTripType> MasterTripType_All(PageParam pageParam, string search)
        {
            return this.abstractMasterTripTypeDao.MasterTripType_All(pageParam, search);
        }
    }
}
    