﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterRelationshipStatusServices : AbstractMasterRelationshipStatusServices
    {
        private AbstractMasterRelationshipStatusDao abstractMasterRelationshipStatusDao;

        public MasterRelationshipStatusServices(AbstractMasterRelationshipStatusDao abstractMasterRelationshipStatusDao)
        {
            this.abstractMasterRelationshipStatusDao = abstractMasterRelationshipStatusDao;
        }

     
        
        public override PagedList<AbstractMasterRelationshipStatus> MasterRelationshipStatus_All(PageParam pageParam, string search)
        {
            return this.abstractMasterRelationshipStatusDao.MasterRelationshipStatus_All(pageParam, search);
        }


        
    }
}
    