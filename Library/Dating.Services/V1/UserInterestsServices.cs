﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class UserInterestsServices : AbstractUserInterestsServices
    {
        private AbstractUserInterestsDao abstractUserInterestsDao;

        public UserInterestsServices(AbstractUserInterestsDao abstractUserInterestsDao)
        {
            this.abstractUserInterestsDao = abstractUserInterestsDao;
        }
        public override PagedList<AbstractUserInterests> UserInterests_ByUserId(PageParam pageParam, string search,int UserId)
        {
            return this.abstractUserInterestsDao.UserInterests_ByUserId(pageParam, search,UserId);
        }
    }
}
    