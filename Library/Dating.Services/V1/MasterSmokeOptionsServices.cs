﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterSmokeOptionsServices : AbstractMasterSmokeOptionsServices
    {
        private AbstractMasterSmokeOptionsDao abstractMasterSmokeOptionsDao;

        public MasterSmokeOptionsServices(AbstractMasterSmokeOptionsDao abstractMasterSmokeOptionsDao)
        {
            this.abstractMasterSmokeOptionsDao = abstractMasterSmokeOptionsDao;
        }
        public override PagedList<AbstractMasterSmokeOptions> MasterSmokeOptions_All(PageParam pageParam, string search)
        {
            return this.abstractMasterSmokeOptionsDao.MasterSmokeOptions_All(pageParam, search);
        }
    }
}
    