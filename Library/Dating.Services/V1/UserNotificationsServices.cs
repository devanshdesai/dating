﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class UserNotificationsServices : AbstractUserNotificationsServices
    {
        private AbstractUserNotificationsDao abstractUserNotificationsDao;

        public UserNotificationsServices(AbstractUserNotificationsDao abstractUserNotificationsDao)
        {
            this.abstractUserNotificationsDao = abstractUserNotificationsDao;
        }

        public override SuccessResult<AbstractUserNotifications> UserNotifications_Upsert(AbstractUserNotifications abstractUserNotifications)
        {
            return this.abstractUserNotificationsDao.UserNotifications_Upsert(abstractUserNotifications);
        }
        public override SuccessResult<AbstractUserNotifications> UserNotifications_MarkReadById(int Id)
        {
            return this.abstractUserNotificationsDao.UserNotifications_MarkReadById(Id);
        }

        public override SuccessResult<AbstractUserNotifications> UserNotifications_MarkReadByUserId(int UserId)
        {
            return this.abstractUserNotificationsDao.UserNotifications_MarkReadByUserId(UserId);
        }
       
        public override PagedList<AbstractUserNotifications> UserNotifications_All(PageParam pageParam, string search)
        {
            return this.abstractUserNotificationsDao.UserNotifications_All(pageParam, search);
        }


        
    }
}
    