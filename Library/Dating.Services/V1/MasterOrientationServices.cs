﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterOrientationServices : AbstractMasterOrientationServices
    {
        private AbstractMasterOrientationDao abstractMasterOrientationDao;

        public MasterOrientationServices(AbstractMasterOrientationDao abstractMasterOrientationDao)
        {
            this.abstractMasterOrientationDao = abstractMasterOrientationDao;
        }
        public override PagedList<AbstractMasterOrientation> MasterOrientation_All(PageParam pageParam, string search)
        {
            return this.abstractMasterOrientationDao.MasterOrientation_All(pageParam, search);
        }
    }
}
    