﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterBodyTypeServices : AbstractMasterBodyTypeServices
    {
        private AbstractMasterBodyTypeDao abstractMasterBodyTypeDao;

        public MasterBodyTypeServices(AbstractMasterBodyTypeDao abstractMasterBodyTypeDao)
        {
            this.abstractMasterBodyTypeDao = abstractMasterBodyTypeDao;
        }
        public override PagedList<AbstractMasterBodyType> MasterBodyType_All(PageParam pageParam, string search)
        {
            return this.abstractMasterBodyTypeDao.MasterBodyType_All(pageParam, search);
        }
    }
}
    