﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterMeetingBeforeTripServices : AbstractMasterMeetingBeforeTripServices
    {
        private AbstractMasterMeetingBeforeTripDao abstractMasterMeetingBeforeTripDao;

        public MasterMeetingBeforeTripServices(AbstractMasterMeetingBeforeTripDao abstractMasterMeetingBeforeTripDao)
        {
            this.abstractMasterMeetingBeforeTripDao = abstractMasterMeetingBeforeTripDao;
        }
        public override PagedList<AbstractMasterMeetingBeforeTrip> MasterMeetingBeforeTrip_All(PageParam pageParam, string search)
        {
            return this.abstractMasterMeetingBeforeTripDao.MasterMeetingBeforeTrip_All(pageParam, search);
        }
    }
}
    