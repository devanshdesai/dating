﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterAccomodationServices : AbstractMasterAccomodationServices
    {
        private AbstractMasterAccomodationDao abstractMasterAccomodationDao;

        public MasterAccomodationServices(AbstractMasterAccomodationDao abstractMasterAccomodationDao)
        {
            this.abstractMasterAccomodationDao = abstractMasterAccomodationDao;
        }
        public override PagedList<AbstractMasterAccomodation> MasterAccomodation_All(PageParam pageParam, string search)
        {
            return this.abstractMasterAccomodationDao.MasterAccomodation_All(pageParam, search);
        }
    }
}
    