﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterIdentityLoginsServices : AbstractMasterIdentityLoginsServices
    {
        private AbstractMasterIdentityLoginsDao abstractMasterIdentityLoginsDao;

        public MasterIdentityLoginsServices(AbstractMasterIdentityLoginsDao abstractMasterIdentityLoginsDao)
        {
            this.abstractMasterIdentityLoginsDao = abstractMasterIdentityLoginsDao;
        }

     
        
        public override PagedList<AbstractMasterIdentityLogins> MasterIdentityLogins_All(PageParam pageParam, string search)
        {
            return this.abstractMasterIdentityLoginsDao.MasterIdentityLogins_All(pageParam, search);
        }


        
    }
}
    