﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterLookingForServices : AbstractMasterLookingForServices
    {
        private AbstractMasterLookingForDao abstractMasterLookingForDao;

        public MasterLookingForServices(AbstractMasterLookingForDao abstractMasterLookingForDao)
        {
            this.abstractMasterLookingForDao = abstractMasterLookingForDao;
        }
        
        public override PagedList<AbstractMasterLookingFor> MasterLookingFor_All(PageParam pageParam, string search)
        {
            return this.abstractMasterLookingForDao.MasterLookingFor_All(pageParam, search);
        }


        
    }
}
    