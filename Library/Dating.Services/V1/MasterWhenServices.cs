﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterWhenServices : AbstractMasterWhenServices
    {
        private AbstractMasterWhenDao abstractMasterWhenDao;

        public MasterWhenServices(AbstractMasterWhenDao abstractMasterWhenDao)
        {
            this.abstractMasterWhenDao = abstractMasterWhenDao;
        }
        public override PagedList<AbstractMasterWhen> MasterWhen_All(PageParam pageParam, string search)
        {
            return this.abstractMasterWhenDao.MasterWhen_All(pageParam, search);
        }
    }
}
    