﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterGenderServices : AbstractMasterGenderServices
    {
        private AbstractMasterGenderDao abstractMasterGenderDao;

        public MasterGenderServices(AbstractMasterGenderDao abstractMasterGenderDao)
        {
            this.abstractMasterGenderDao = abstractMasterGenderDao;
        }

     
        
        public override PagedList<AbstractMasterGender> MasterGender_All(PageParam pageParam, string search)
        {
            return this.abstractMasterGenderDao.MasterGender_All(pageParam, search);
        }


        
    }
}
    