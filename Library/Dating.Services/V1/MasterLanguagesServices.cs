﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterLanguagesServices : AbstractMasterLanguagesServices
    {
        private AbstractMasterLanguagesDao abstractMasterLanguagesDao;

        public MasterLanguagesServices(AbstractMasterLanguagesDao abstractMasterLanguagesDao)
        {
            this.abstractMasterLanguagesDao = abstractMasterLanguagesDao;
        }

     
        
        public override PagedList<AbstractMasterLanguages> MasterLanguages_All(PageParam pageParam, string search)
        {
            return this.abstractMasterLanguagesDao.MasterLanguages_All(pageParam, search);
        }


        
    }
}
    