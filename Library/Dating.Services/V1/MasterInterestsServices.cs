﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterInterestsServices : AbstractMasterInterestsServices
    {
        private AbstractMasterInterestsDao abstractMasterInterestsDao;

        public MasterInterestsServices(AbstractMasterInterestsDao abstractMasterInterestsDao)
        {
            this.abstractMasterInterestsDao = abstractMasterInterestsDao;
        }

     
        
        public override PagedList<AbstractMasterInterests> MasterInterests_All(PageParam pageParam, string search)
        {
            return this.abstractMasterInterestsDao.MasterInterests_All(pageParam, search);
        }


        
    }
}
    