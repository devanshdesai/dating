﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class UserGalleryServices : AbstractUserGalleryServices
    {
        private AbstractUserGalleryDao abstractUserGalleryDao;

        public UserGalleryServices(AbstractUserGalleryDao abstractUserGalleryDao)
        {
            this.abstractUserGalleryDao = abstractUserGalleryDao;
        }

        public override SuccessResult<AbstractUserGallery> UserGallery_Upsert(AbstractUserGallery abstractUserGallery)
        {
            return this.abstractUserGalleryDao.UserGallery_Upsert(abstractUserGallery);
        }

        public override SuccessResult<AbstractUserGallery> UserGallery_Delete(int Id,int DeletedBy)
        {
            return this.abstractUserGalleryDao.UserGallery_Delete(Id, DeletedBy);
        }
        
        public override PagedList<AbstractUserGallery> UserGallery_All(PageParam pageParam, string search)
        {
            return this.abstractUserGalleryDao.UserGallery_All(pageParam, search);
        }

        public override PagedList<AbstractUserGallery> UserGallery_ByUserId(PageParam pageParam, string search,int UserId)
        {
            return this.abstractUserGalleryDao.UserGallery_ByUserId(pageParam, search, UserId);
        }

    }

    }
    