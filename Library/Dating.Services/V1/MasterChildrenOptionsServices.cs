﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterChildrenOptionsServices : AbstractMasterChildrenOptionsServices
    {
        private AbstractMasterChildrenOptionsDao abstractMasterChildrenOptionsDao;

        public MasterChildrenOptionsServices(AbstractMasterChildrenOptionsDao abstractMasterChildrenOptionsDao)
        {
            this.abstractMasterChildrenOptionsDao = abstractMasterChildrenOptionsDao;
        }
        public override PagedList<AbstractMasterChildrenOptions> MasterChildrenOptions_All(PageParam pageParam, string search)
        {
            return this.abstractMasterChildrenOptionsDao.MasterChildrenOptions_All(pageParam, search);
        }
    }
}
    