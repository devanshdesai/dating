﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class UserLanguagesServices : AbstractUserLanguagesServices
    {
        private AbstractUserLanguagesDao abstractUserLanguagesDao;

        public UserLanguagesServices(AbstractUserLanguagesDao abstractUserLanguagesDao)
        {
            this.abstractUserLanguagesDao = abstractUserLanguagesDao;
        }
        public override PagedList<AbstractUserLanguages> UserLanguages_ByUserId(PageParam pageParam, string search,int UserId)
        {
            return this.abstractUserLanguagesDao.UserLanguages_ByUserId(pageParam, search,UserId);
        }
    }
}
    