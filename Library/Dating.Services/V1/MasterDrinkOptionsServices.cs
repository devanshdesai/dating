﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterDrinkOptionsServices : AbstractMasterDrinkOptionsServices
    {
        private AbstractMasterDrinkOptionsDao abstractMasterDrinkOptionsDao;

        public MasterDrinkOptionsServices(AbstractMasterDrinkOptionsDao abstractMasterDrinkOptionsDao)
        {
            this.abstractMasterDrinkOptionsDao = abstractMasterDrinkOptionsDao;
        }

     
        
        public override PagedList<AbstractMasterDrinkOptions> MasterDrinkOptions_All(PageParam pageParam, string search)
        {
            return this.abstractMasterDrinkOptionsDao.MasterDrinkOptions_All(pageParam, search);
        }


        
    }
}
    