﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterStateServices : AbstractMasterStateServices
    {
        private AbstractMasterStateDao abstractMasterStateDao;

        public MasterStateServices(AbstractMasterStateDao abstractMasterStateDao)
        {
            this.abstractMasterStateDao = abstractMasterStateDao;
        }

     
        
        public override PagedList<AbstractMasterState> MasterState_All(PageParam pageParam, string search)
        {
            return this.abstractMasterStateDao.MasterState_All(pageParam, search);
        }


        
    }
}
    