﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterCityServices : AbstractMasterCityServices
    {
        private AbstractMasterCityDao abstractMasterCityDao;

        public MasterCityServices(AbstractMasterCityDao abstractMasterCityDao)
        {
            this.abstractMasterCityDao = abstractMasterCityDao;
        }
        
        public override PagedList<AbstractMasterCity> MasterCity_All(PageParam pageParam, string search, int StateId)
        {
            return this.abstractMasterCityDao.MasterCity_All(pageParam, search, StateId);
        }


        
    }
}
    