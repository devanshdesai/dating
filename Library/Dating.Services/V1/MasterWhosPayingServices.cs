﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterWhosPayingServices : AbstractMasterWhosPayingServices
    {
        private AbstractMasterWhosPayingDao abstractMasterWhosPayingDao;

        public MasterWhosPayingServices(AbstractMasterWhosPayingDao abstractMasterWhosPayingDao)
        {
            this.abstractMasterWhosPayingDao = abstractMasterWhosPayingDao;
        }
        public override PagedList<AbstractMasterWhosPaying> MasterWhosPaying_All(PageParam pageParam, string search)
        {
            return this.abstractMasterWhosPayingDao.MasterWhosPaying_All(pageParam, search);
        }
    }
}
    