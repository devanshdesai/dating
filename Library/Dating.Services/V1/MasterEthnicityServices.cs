﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterEthnicityServices : AbstractMasterEthnicityServices
    {
        private AbstractMasterEthnicityDao abstractMasterEthnicityDao;

        public MasterEthnicityServices(AbstractMasterEthnicityDao abstractMasterEthnicityDao)
        {
            this.abstractMasterEthnicityDao = abstractMasterEthnicityDao;
        }
        public override PagedList<AbstractMasterEthnicity> MasterEthnicity_All(PageParam pageParam, string search)
        {
            return this.abstractMasterEthnicityDao.MasterEthnicity_All(pageParam, search);
        }
    }
}
    