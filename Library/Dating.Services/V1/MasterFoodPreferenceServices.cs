﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterFoodPreferenceServices : AbstractMasterFoodPreferenceServices
    {
        private AbstractMasterFoodPreferenceDao abstractMasterFoodPreferenceDao;

        public MasterFoodPreferenceServices(AbstractMasterFoodPreferenceDao abstractMasterFoodPreferenceDao)
        {
            this.abstractMasterFoodPreferenceDao = abstractMasterFoodPreferenceDao;
        }
        public override PagedList<AbstractMasterFoodPreference> MasterFoodPreference_All(PageParam pageParam, string search)
        {
            return this.abstractMasterFoodPreferenceDao.MasterFoodPreference_All(pageParam, search);
        }
    }
}
    