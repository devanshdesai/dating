﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Data.Contract;
using Dating.Entities.Contract;
using Dating.Services.Contract;

namespace Dating.Services.V1
{
    public class MasterTravelViaServices : AbstractMasterTravelViaServices
    {
        private AbstractMasterTravelViaDao abstractMasterTravelViaDao;

        public MasterTravelViaServices(AbstractMasterTravelViaDao abstractMasterTravelViaDao)
        {
            this.abstractMasterTravelViaDao = abstractMasterTravelViaDao;
        }
        public override PagedList<AbstractMasterTravelVia> MasterTravelVia_All(PageParam pageParam, string search)
        {
            return this.abstractMasterTravelViaDao.MasterTravelVia_All(pageParam, search);
        }
    }
}
    