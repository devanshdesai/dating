﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dating.Services
{
    using Autofac;
    using Data;
    using Dating.Services.Contract;

    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DataModule>();
            builder.RegisterType<V1.MasterSmokeOptionsServices>().As<AbstractMasterSmokeOptionsServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterBodyTypeServices>().As<AbstractMasterBodyTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterEthnicityServices>().As<AbstractMasterEthnicityServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterOrientationServices>().As<AbstractMasterOrientationServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterFoodPreferenceServices>().As<AbstractMasterFoodPreferenceServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterChildrenOptionsServices>().As<AbstractMasterChildrenOptionsServices>().InstancePerDependency();
            builder.RegisterType<V1.UserGalleryServices>().As<AbstractUserGalleryServices>().InstancePerDependency();
            builder.RegisterType<V1.UserLanguagesServices>().As<AbstractUserLanguagesServices>().InstancePerDependency();
            builder.RegisterType<V1.UserInterestsServices>().As<AbstractUserInterestsServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterTripTypeServices>().As<AbstractMasterTripTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterWhenServices>().As<AbstractMasterWhenServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterWhosPayingServices>().As<AbstractMasterWhosPayingServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterTravelViaServices>().As<AbstractMasterTravelViaServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterMeetingBeforeTripServices>().As<AbstractMasterMeetingBeforeTripServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterAccomodationServices>().As<AbstractMasterAccomodationServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterGenderServices>().As<AbstractMasterGenderServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterInterestsServices>().As<AbstractMasterInterestsServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterCountryServices>().As<AbstractMasterCountryServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterStateServices>().As<AbstractMasterStateServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterCityServices>().As<AbstractMasterCityServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterLanguagesServices>().As<AbstractMasterLanguagesServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterLookingForServices>().As<AbstractMasterLookingForServices>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationsServices>().As<AbstractUserNotificationsServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterIdentityLoginsServices>().As<AbstractMasterIdentityLoginsServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterRelationshipStatusServices>().As<AbstractMasterRelationshipStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterDrinkOptionsServices>().As<AbstractMasterDrinkOptionsServices>().InstancePerDependency();
            base.Load(builder);
        }
    }
}
