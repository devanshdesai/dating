﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Entities.Contract;

namespace Dating.Services.Contract
{
    public abstract class AbstractMasterGenderServices
    {
     
        public abstract PagedList<AbstractMasterGender> MasterGender_All(PageParam pageParam, string search);
  

    }
}
