﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Entities.Contract;

namespace Dating.Services.Contract
{
    public abstract class AbstractMasterTripTypeServices
    {
        public abstract PagedList<AbstractMasterTripType> MasterTripType_All(PageParam pageParam, string search);
    }
}
