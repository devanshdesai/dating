﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using Dating.Common.Paging;
using Dating.Entities.Contract;

namespace Dating.Services.Contract
{
    public abstract class AbstractUserGalleryServices
    {
        public abstract SuccessResult<AbstractUserGallery> UserGallery_Upsert(AbstractUserGallery abstractUserGallery);
        public abstract SuccessResult<AbstractUserGallery> UserGallery_Delete(int Id,int DeletedBy);
        public abstract PagedList<AbstractUserGallery> UserGallery_ByUserId(PageParam pageParam, string search,int UserId);
        public abstract PagedList<AbstractUserGallery> UserGallery_All(PageParam pageParam, string search);
    }
}
